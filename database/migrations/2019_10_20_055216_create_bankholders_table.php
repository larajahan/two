<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankholdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bankholders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bank_name');
            $table->string('bank_holder_name')->nullable();
            $table->string('account_no')->nullable();
            $table->string('address')->nullable();
            $table->string('know_person')->nullable();
            $table->string('designation')->nullable();
            $table->string('mobile')->nullable();
            $table->string('opening_blance')->nullable();
            $table->string('balance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bankholders');
    }
}
