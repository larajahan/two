@extends('layouts.app')
@section('content')
    <script
            src="https://code.jquery.com/jquery-3.4.1.slim.js"
            integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
            crossorigin="anonymous"></script>


    <div class="content_wrapper">

        <div class="middle_content_wrapper">
            <section class="page_content">
                <div class="panel mb-0">
                    <div class="panel_header">
                        <div class="panel_title">
                            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span> Employee Attendance</span>
                        </div>
                    </div>
                    <div class="panel_body">

                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <form action="{{route('maneger.employee.attendance.result')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleSelect1">Select Employee</label>
                                        <select name="employee" class="form-control" id="employee">
                                            @foreach($employees as $emp)
                                                <option value="{{$emp->id}}">{{$emp->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleSelect1">Select Month</label>
                                        <select name="month" class="form-control">
                                            <option value="">select</option>
                                            <option value="January">Janyary</option>
                                            <option value="February">February</option>
                                            <option value="October">October</option>
                                            <option value="November">November</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success">Show</button>


                                </form>

                            </div>


                        </div>


                    </div>
                </div> <!--/ panel body -->


            </section>
            <!--/ page content -->
            <!-- start code here... -->

        </div><!--/middle content wrapper-->
    </div><!--/ content wrapper -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="branch"]').on('change', function(){
                var branch_id = $(this).val();
                if(branch_id) {
                    $.ajax({
                        url: "{{  url('/get/employee/') }}/"+branch_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data) {
                            $('#employee').empty();
                            $('#employee').append(' <option value="0">--Select--</option>');
                            $.each(data,function(index,empObj){
                                $('#employee').append('<option value="' + empObj.id + '">'+empObj.name+'</option>');
                            });
                        }
                    });
                } else {
                    alert('danger');
                }
            });
        });
        </script>


@endsection

