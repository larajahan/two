@extends('layouts.app')
@section('content')
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
<!-- content wrpper -->
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Add Employee</span>
						<a href="{{route('admin.employee.create')}}" class="btn btn-success float-right">Create Employee</a>
					</div>
				</div>
				<div class="panel_body">
					<div class="row">
						<div class="col-md-10 col-xs-12 offset-1">
							<div >
								<div class="card">
									<h5 class="card-header">Add Employee</h5>
									<div class="card-body">
										@if ($errors->all())
										<div class="alert alert-danger">
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</div>
										@endif
										<form action="{{route('admin.employee.store')}}" enctype="multipart/form-data" method="post">
											@csrf
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">

														<label for="exampleSelect1">Branch</label>
														<select class="form-control" name="branch">
															@foreach($branchs as $branch)
																<option value="{{$branch->id}}">{{$branch->name}}</option>
															@endforeach
															<option value="master">Main Branch</option>

														</select>

													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Employee Name</label>
														<input type="text" class="form-control"  name="name">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Email</label>
														<input type="text" class="form-control"  name="email">
													</div>
												</div>

													<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Mobile</label>
														<input type="mobile" class="form-control"  name="mobile" >
													       </div>
												     </div>
												<div class="col-md-4 ccol-xs-12">
													<div class="form-group">
														<label> Address</label>
														<input type="text" class="form-control" name="address" >
													</div>
												</div>
												     <div class="col-md-4 col-xs-12">
                                                       <div class="form-group">
                                                          <label>Salary</label>
														   <input type="number" class="form-control" name="salary" value="" id="salary" onkeyup="sum();">
                                                      </div>
												    </div>


												
											</div>
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Fax</label>
														<input type="number" class="form-control"  name="fax" value="">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													
													<div class="form-group">
														<label>Contact person</label>
														<input type="text" class="form-control"  name="contact_person" value="">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Per Day</label>
														<input type="number" class="form-control" name="perday" id="perday"  >
													</div>
												</div>

											</div>
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Company person</label>
														<input type="text" class="form-control"  name="company_person" value="">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Bank name</label>
														<input type="text" class="form-control"  name="bank_name" value="">
													</div>
												</div>

												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Account number</label>
														<input type="number" class="form-control"  name="account_number" value="">
													</div>
												</div>
												
											</div>
											
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Account name</label>
														<input type="text" class="form-control"  name="account_name" value="">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Bank Branch</label>
														<input type="text" class="form-control"  name="bank_branch">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Position</label>
														<select class="form-control"  name="position">
															<option selected>Select</option>
															<option value="Employee">Employee</option>
															<option value="manager">manager</option>

														</select>
													</div>
												</div>


												

											</div>

											<div class="form-row">
												<div class="col-md-6 col-xs-12">
													<div class="form-group">
														<label>Upload Photo</label>
														<input type="file" class="form-control"  name="image" >
													</div>
												</div>
												<div class="col-md-6 col-xs-12">
													
												</div>
												
												
											</div>
											
											<button type="submit" class="btn btn-primary">Add Employee</button>
										</form>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
				</div> <!--/ panel body -->
				</div><!--/ panel -->
			</section>
			<!--/ page content -->
			<!-- start code here... -->
			</div><!--/middle content wrapper-->
			</div><!--/ content wrapper -->
			<script type="text/javascript">
           function sum() {
               var salary = document.getElementById('salary').value;
                var result = parseInt(salary) /28;
               if (!isNaN(result)) {
               document.getElementById('perday').value = parseInt(result);
               }
              }
             </script>
			@endsection