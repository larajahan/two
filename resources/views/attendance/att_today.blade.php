@extends('layouts.app')
@section('content')


    <div class="content_wrapper">

        <div class="middle_content_wrapper">
            <section class="page_content">
                <div class="panel mb-0">
                    <div class="panel_header">
                        <div class="panel_title">
                            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Today Attendance</span>
                        </div>
                    </div>
                    <div class="panel_body">

                        <div class="row">
                            <div class="col-md-12">
                                <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                                    <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Employee Name</th>
                                        <th>Branch</th>
                                        <th>Mobile</th>
                                        <th>Image</th>
                                        <th>Date</th>
                                        <th class="text-center" >Attendance</th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($att_today as $att_t)
                                        <tr>


                                            <td>{{$att_t->employee->employee_id}}</td>
                                            <td>{{$att_t->employee->name}}</td>
                                            @if($att_t->branch =='master')
                                                <td>Master</td>
                                            @else
                                                <td>{{$att_t->branchas->name}}</td>
                                            @endif

                                            <td>{{$att_t->employee->mobile}}</td>

                                            <td><img id="logo" src="{{asset('public/panel/employee/'.$att_t->employee->image) }}" width="50" height="50;" /></td>
                                            <td>{{$att_t->date}}</td>

                                            <td>
                                                <fieldset class="form-group">

                                                    <div>
                                                        <label class="form-check-label">
                                                            <input disabled type="radio" id="optionsRadios1" value="present" checked="">
                                                            {{$att_t -> attendance}} &nbsp;&nbsp;
                                                        </label>
                                                    </div>

                                                </fieldset>
                                            </td>





                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>

                                <a class="btn btn-success" href="{{route('maneger.att.edit')}}">update</a>

                            </div>





                        </div>


                    </div>
                </div> <!--/ panel body -->


            </section>
            <!--/ page content -->
            <!-- start code here... -->

        </div><!--/middle content wrapper-->
    </div><!--/ content wrapper -->


@endsection
