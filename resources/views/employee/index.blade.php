@extends('layouts.app')
@section('content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->

      <!-- panel -->
         <div class="panel mb-0">


        <div class="panel_header">

          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Employee</span>
          </div>
        </div>
        <div class="panel_body">

          <div class="table-responsive">
            <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
              <thead>
                <tr>
                  <th>Employee ID</th>
                  <th>Employee Name</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Mobile</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($employees as $employee)
                <tr>
                  <td >{{$employee->id}}</td>
                  <td>{{$employee->name}}</td>
                  <td>{{$employee->email}}</td>
                  <td>{{$employee->address}}</td>
                  <td>{{$employee->mobile}}</td>
                  <td><img id="logo" src="{{asset('public/panel/employee/'.$employee->image) }}" width="50" height="50;" /></td>                 
                  <td>
                    <div class="btn-group" role="group">
                      <a href="{{ route('employee.view', $employee->id) }}" class="btn btn-sm btn-secondary">View</a>
                      <a href="{{ route('employee.delete', $employee->id) }}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
                      <a href="{{ route('employee.edit', $employee->id) }}" class="btn btn-sm btn-primary">Edit</a>


                    </div>
                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>

          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->

        @endsection
