@extends('layouts.app')
@section('content')
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
<!-- content wrpper -->
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Edit Employee</span>
					</div>
				</div>
				<div class="panel_body">
					<div class="row">
						<div class="col-md-10 col-xs-12 offset-1">
							<div >
								<div class="card">
									<h5 class="card-header">Edit Employee Form</h5>
									<div class="card-body">
										@if ($errors->all())
										<div class="alert alert-danger">
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</div>
										@endif
										<form action="{{route('employee.update',$employee->id)}}" enctype="multipart/form-data" method="post">
											@csrf
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Employee Name</label>
														<input type="text" class="form-control"  name="name" value="{{$employee->name}}" >
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Email</label>
														<input type="text" class="form-control"  name="email" value="{{$employee->email}}">
													</div>
												</div>
												<div class="col-md-4 ccol-xs-12">
													<div class="form-group">
														<label> Address</label>
														<input type="text" class="form-control" name="address" value="{{$employee->address}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Mobile</label>
														<input type="mobile" class="form-control"  name="mobile" value="{{$employee->mobile}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Salary</label>
														<input type="number" class="form-control" name="salary" value="{{$employee->salary}}" id="salary" onkeyup="sum();">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Per Day</label>
														<input type="number" class="form-control" name="perday" id="perday" value="{{$employee->perday}}" >
													</div>
												</div>
												
											</div>
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Fax</label>
														<input type="number" class="form-control"  name="fax" value="{{$employee->perday}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													
													<div class="form-group">
														<label>Contact person</label>
														<input type="text" class="form-control"  name="contact_person" value="{{$employee->perday}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Position</label>
														<select class="form-control"  name="position">
															<option selected>Select</option>
															<option value="value="{{$employee->position}}">Employee</option>
															<option value="manager">manager</option>
															
														</select>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Company person</label>
														<input type="text" class="form-control"  name="company_person" value="{{$employee->company_person}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Bank name</label>
														<input type="text" class="form-control"  name="bank_name" value="{{$employee->bank_name}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Account number</label>
														<input type="number" class="form-control"  name="account_number" value="{{$employee->account_number}}">
													</div>
												</div>
												
											</div>
											
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Account name</label>
														<input type="text" class="form-control"  name="account_name" value="{{$employee->account_name}}">
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Branch</label>
														<input type="text" class="form-control"  name="branch" value="{{$employee->branch}}">
													</div>
												</div>
												
											</div>
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Upload Logo</label>
														<img src="" id="show">
														<input type="file"   name="image" id="cust_image" onchange="readURL(this);">
														<!-- <img src="" id="show1">
														<input type="file"   name="image" id="cust_image" onchange="readURL1(this);"> -->
													</div>
													<input type="hidden" name="oldpic" value="{{$employee->image }}">
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<img src="{{ URL::to('public/panel/employee/'.$employee->image) }}" width="100" height="100" />
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
												</div>
											</div>
											<button type="submit" class="btn btn-primary">Update Employee</button>
										</form>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
				</div> <!--/ panel body -->
				</div><!--/ panel -->
			</section>
			<!--/ page content -->
			<!-- start code here... -->
			</div><!--/middle content wrapper-->
			</div><!--/ content wrapper -->
			<script type="text/javascript">
			function sum() {
			var salary = document.getElementById('salary').value;
			var result = parseInt(salary) /28;
			if (!isNaN(result)) {
			document.getElementById('perday').value = parseInt(result);
			}
			}
			</script>
			<script>
			function readURL(input) {
			if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e)
			{
			$('#show')
			.attr('src', e.target.result)
			.width(80)
			.height(80);
			};
			reader.readAsDataURL(input.files[0]);
			}
			}
			</script>
			<!-- <script>
			function readURL1(input) {
			if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e)
			{
			$('#show1')
			.attr('src', e.target.result)
			.width(80)
			.height(80);
			};
			reader.readAsDataURL(input.files[0]);
			}
			}
			</script> -->
			@endsection