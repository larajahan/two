@extends('layouts.app')
@section('content')
<div class="container mt-3">
  <div class="row">
    <div class="col-md-12 ">
      <div class="card">
        <div class="card-header bg-primary text-white">
          Update Customer
        </div>
        <div class="card-body">
          <form action="{{ url('/edit/customer/update') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label >Customer Name</label>
                  <input type="hidden" class="form-control"  name="id" value="{{ $custedit->id}}" >
                  <input type="text" class="form-control"  name="cust_name" value="{{ $custedit->cust_name}}" >
                </div>
              </div>
              <div class="col-md-4 ccol-xs-12">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email" value="{{ $custedit->email}}">
                </div>
              </div>
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label>Address</label>
                  <input type="text" class="form-control"  name="address" value="{{ $custedit->address}}">
                </div>
              </div>        
            </div>

           <div class="form-row">
             <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label>Mobile</label>
                    <input type="text" class="form-control"  name="mobile" value="{{ $custedit->mobile}}">
                  </div>
             </div> 
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label>Bank Name</label>
                    <input type="text" class="form-control"  name="bank_name" value="{{ $custedit->bank_name}}">
                  </div>
             </div> 
             <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label>Account Name</label>
                    <input type="text" class="form-control"  name="account_name" value="{{ $custedit->account_name}}">
                  </div>
             </div> 
            
           </div>     
            
            
             <div class="form-row">
                <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label>Account NO</label>
                    <input type="text" class="form-control"  name="account_no" value="{{ $custedit->account_no}}">
                  </div>
             </div>
                <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <label>Brance Name</label>
                    <input type="text" class="form-control"  name="brance_name" value="{{ $custedit->brance_name}}">
                  </div>
                </div>
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label>Opening Balance</label>
                  <input type="text" class="form-control"  name="open_blance" value="{{ $custedit->open_blance ?: old('open_blance')}}">
                </div>
              </div>              
            </div>
             <div class="form-row">               
               <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <label>Upload Logo</label> 
                   <img src="" id="show">                     
                  <input type="file"   name="cust_image"id="cust_image" onchange="readURL(this);">
                </div>                           
                <input type="hidden" name="oldpic" value="{{$custedit->cust_image }}">
              </div>             
              <div class="col-md-4 col-xs-12">
                <div class="form-group">
                  <img src="{{ URL::to('public/panel/customer/'.$custedit->cust_image) }}" width="100" height="100" />                   
                </div>               
              </div>

               <div class="col-md-4 col-xs-12">
               </div>
             </div>

            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </div>
    </div>
  </div>

   <script >
            function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) 
                {
                $('#show')
                  .attr('src', e.target.result)
                  .width(80)
                  .height(80);
                       };
            reader.readAsDataURL(input.files[0]);
            }
        }
        </script>

   
  @endsection