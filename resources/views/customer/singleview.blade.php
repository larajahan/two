@extends('layouts.app')
@section('content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
        <div class="row"> 
        <div class="col-md-8 offset-1"> 
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Single Customer</span>
          </div>
        </div>
        <div class="panel_body">       
       

                  <div class="row">
            <div class="col-md-4 ">
              <div class="profile">
                {{-- <img src="https://www.gravatar.com/avatar/e4d6f769d84602ac115aa4c29797ba25?s=250" alt=""  class="img-fluid rounded-circle"> --}}
                <img id="logo" src="{{asset('public/panel/customer/'.$single_view->cust_image) }}" alt=""  class="img-fluid rounded-circle" />
              </div>
            </div>
            <div class="col-md-8">
              <div class="user_info">
                <div class="table-responsive">
                  <table class="table table-hover mt-2">                  
                    <tbody>
                      <tr>
                        <td class="font-weight-bold">Customer ID:</td>
                         <td>{{$single_view->customer_id}}</td>
                      </tr>
                      <tr>
                        <tr>
                        <td class="font-weight-bold">Customer Name:</td>
                         <td>{{$single_view->cust_name}}</td>
                      </tr>
                      <tr>
                        <td class="font-weight-bold">Email:</td>
                        <td>{{$single_view->email}}</td>
                      </tr>
                      <tr>
                        <td class="font-weight-bold">Address:</td>
                        <td>{{$single_view->address}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Mobile:</td>
                        <td>{{$single_view->mobile}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Bank Name:</td>
                        <td>{{$single_view->bank_name}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Account Name:</td>
                        <td>{{$single_view->account_name}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Account NO:</td>
                        <td>{{$single_view->account_no}}</td>
                      </tr>
                       <tr>
                        <td class="font-weight-bold">Brance Name:</td>
                        <td>{{$single_view->brance_name}}</td>
                      </tr> 
                      <tr>
                        <td class="font-weight-bold">Opening Blance:</td>
                        <td>{{$single_view->open_blance}}</td>
                      </tr>
                      <tr class="font-weight-bold">                        
                         <td><a class="btn btn-info btn-sm" href="{{route('list.customer')}}">Back</a></td>

                      </tr>

                    </tbody>   
                              
                  </table>               
            </div>         
          </div>


            </div> <!--/ panel body -->
            </div><!--/ panel -->
              </div>         
          </div>
           
          </section>
          <!--/ page content -->
          <!-- start code here... -->
          </div><!--/middle content wrapper-->
          </div><!--/ content wrapper -->
          @endsection