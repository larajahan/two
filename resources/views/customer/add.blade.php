@extends('layouts.app')
@section('content')
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
<!-- content wrpper -->
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Add Customer</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							<div >
								<div class="card">
									<h5 class="card-header">Customer Form</h5>
									<div class="card-body">
										@if ($errors->all())
										<div class="alert alert-danger">
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</div>
										@endif
										<form action="{{ url('/insert/customer') }}" enctype="multipart/form-data" method="post">
											@csrf
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Customer Name</label>
														<input type="text" class="form-control @error('cust_name') is-invalid @enderror"  name="cust_name" value="">
														@error('cust_name')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												<div class="col-md-4 ccol-xs-12">
													<div class="form-group">
														<label>Email</label>
														<input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="">
														@error('email')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Address</label>
														<input type="text" class="form-control @error('address') is-invalid @enderror"  name="address" value="">
														@error('address')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Mobile</label>
														<input type="text" class="form-control @error('mobile') is-invalid @enderror"  name="mobile" value="">
														@error('mobile')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Bank Name</label>
														<input type="text" class="form-control @error('bank_name') is-invalid @enderror"  name="bank_name" value="">
														@error('bank_name')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												<div class="col-md-4 ccol-xs-12">
													<div class="form-group">
														<label>Account Name</label>
														<input type="text" class="form-control @error('account_name') is-invalid @enderror" name="account_name" value="">
														@error('account_name')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												
											</div>
											<div class="form-row">
												
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Account NO</label>
														<input type="text" class="form-control @error('account_no') is-invalid @enderror"  name="account_no" value="">
														@error('account_no')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Brance Name</label>
														<input type="text" class="form-control @error('brance_name') is-invalid @enderror"  name="brance_name" value="">
														@error('brance_name')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												<div class="col-md-4 ccol-xs-12">
													<div class="form-group">
														<label>Opening Blance</label>
														<input type="text" class="form-control @error('open_blance') is-invalid @enderror" name="open_blance" value="0">
														@error('open_blance')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
												
											</div>
											<div class="form-row">
												
												
												<div class="col-md-4 col-xs-12">
													<div class="form-group">
														<label>Upload Photo</label>
														<input type="file" class="form-control @error('cust_image') is-invalid @enderror"  name="cust_image" >
														@error('cust_image')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
														@enderror
													</div>
												</div>
											</div>
											
											<button type="submit" class="btn btn-primary">Add Customer</button>
										</form>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
				</div> <!--/ panel body -->
				</div><!--/ panel -->
			</section>
			<!--/ page content -->
			<!-- start code here... -->
			</div><!--/middle content wrapper-->
			</div><!--/ content wrapper -->
			
			@endsection