@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Edit Product</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="row">
            <div class="col-md-10 col-xs-12 offset-1">
              <div >
                <div class="card">
                  <h5 class="card-header">Product Edit Form</h5>
                  <div class="card-body">
                    @if ($errors->all())
                    <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </div>
                    @endif
                    <form action="{{route('admin.product.update',$product->id)}}" enctype="multipart/form-data" method="post">
                      @csrf
                      <div class="form-row">
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" class="form-control"  name="product_name"  required="" value="{{$product->product_name}}">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Category</label>
                             @php
                          $var=App\Category::orderBy('name', 'asc')->get();

                            @endphp

                        <select class="form-control" name="category_id">
                        <option value="">Please select a brand </option>
                         @foreach ($var as $br)
                          <option value="{{ $br->id }}"@if($product->category_id==$br->id) selected @endif>{{ $br->name }}</option>
                         @endforeach
                      </select>
                          </div>
                        </div>
                        <div class="col-md-4 ccol-xs-12">
                          <div class="form-group">
                            <label> Brand</label>
                            @php
                          $var=App\Brand::orderBy('name', 'asc')->get();

                            @endphp
                            <select class="form-control" name="brand_id">
                         <option value="">Please select a brand </option>
                          @foreach ($var as $br)
                          <option value="{{ $br->id }}"@if($product->brand_id==$br->id) selected @endif>{{ $br->name }}</option>
                          @endforeach
                        </select>
                          </div>
                          </div>
                          <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Unit</label>
                            @php
                            $var=App\Unit::orderBy('name', 'asc')->get();

                            @endphp
                            <select class="form-control" name="unit_id">
                           <option value="">Please select a unit </option>
                            @foreach ($var as $unit)
                           <option value="{{ $unit->id }}"@if($product->unit_id==$unit->id) selected @endif>{{ $unit->name }}</option>
                          @endforeach
                          </select>
                            </div>
                             </div>
                             <div class="col-md-4 col-xs-12">
                               <div class="form-group">
                                 <label>Buy Price</label>
                                  <input type="number" class="form-control" name="buy_price" value="{{$product->buy_price}}" required="">
                                    </div>
                                 </div>

                             <div class="col-md-4 col-xs-12">
                               <div class="form-group">
                                  <label>Sell Price</label>
                               <input type="number" class="form-control" name="sell_price" required="" value="{{$product->sell_price}}">
                              </div>
                            </div>
                        
                      </div>
                      <div class="form-row">
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Vat and Tax</label>
                            <input type="number" class="form-control"  name="vat" value="{{$product->vat}}">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          
                          <div class="form-group">
                            <label>Low Stok</label>
                            <input type="number" class="form-control"  name="low_stock" value="{{$product->low_stock}}">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          
                          <div class="form-group">
                            <label>Up Stok</label>
                            <input type="text" class="form-control"  name="up_stock" value="{{$product->up_stock}}">
                          </div>
                        </div>
                        
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-xs-12">
                          <div class="form-group">
                            <label>Product Code</label>
                            <input type="text" class="form-control"  name="product_code" value="{{$product->product_code}}" required="">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Status</label>
                            <select class="form-control"  name="status">
                              <option value="1" <?php  if ($product->status == 1) {
                                echo "selected";
                              } ?> >Active</option>
                              <option value="0" <?php  if ($product->status == 0) {
                                echo "selected";
                              } ?>>Inactive</option>
                            </select>
                          </div>
                        </div>
                      </div>
               
                    <td><img id="logo" src="{{asset('public/panel/product/'.$product->image) }}" width="50" height="50;" /></td>  
                        <div class="form-row">
                        <div class="col-md-6 col-xs-12">
                          <div class="form-group">
                            <label>Upload Photo</label>
                            <input type="file" class="form-control"  name="image" >
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                        </div>
                      </div>
                        
                      <button type="submit" class="btn btn-primary">Update Product</button>
                    </form>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          
        </div>
        </div> <!--/ panel body -->
        </div><!--/ panel -->
      </section>
      <!--/ page content -->
      <!-- start code here... -->
      </div><!--/middle content wrapper-->
      </div><!--/ content wrapper -->
      
      @endsection