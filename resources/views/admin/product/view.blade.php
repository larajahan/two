@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<!-- content wrpper -->
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>View Product</span>
          </div>
        </div>
        <div class="panel_body">

         <div class="row">
          <div class="col-md-6 border-right">
           <p><strong>Product ID : </strong>{{$product->product_code}}</p>
           <p><strong>Product Name : </strong>{{$product->product_name}}</p>
            <p><strong> Category : </strong>{{$product->category->name}}</p>
            <p><strong> Brand : </strong>{{$product->brand_id}}</p>
            <p><strong> Unit : </strong>{{$product->unit_id}}</p>
            <p><strong> Buy Price : </strong>{{$product->buy_price}}</p>
            <p><strong> Sell Price  : </strong>{{$product->sell_price}}</p>
            <p><strong> Stock : </strong>{{$product->stock}}</p>
          </div>
          <div class="col-md-6">
            <p><strong> Status: </strong> {{$product->status}}</p>
            <p><strong>  Barcodee: </strong> {{$product->barcode}}</p>
          <img id="logo" src="{{asset('public/panel/product/'.$product->image) }}" width="50" height="50;" />

             <div>
              <a href="{{route('admin.products')}}" class="btn btn-sm btn-primary">Back</a>

             </div>
             
          </div>
        </div>

            
          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->

        @endsection

       