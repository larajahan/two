@extends('admin.admin_layouts')

@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Add Product</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="row">
            <div class="col-md-10 col-xs-12 offset-1">
              <div >
                <div class="card">
                  <h5 class="card-header">Product Form
                  <span style="float: right;"> <input type="checkbox" id="havecode" onchange="myFunction()"> I have a product code </span>
                  </h5>
                  <div class="card-body">
                    @if ($errors->all())
                    <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </div>
                    @endif

                    <form action="{{route('admin.product.store')}}" enctype="multipart/form-data" method="post">
                      @csrf
                      <div class="form-row">
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" class="form-control"  name="product_name"  required="">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Category</label>
                          <select class="form-control" name="category_id">
                         <option value="">Please select a Category </option>
                         @foreach (App\Category::orderBy('name', 'asc')->get() as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                         @endforeach
                      </select>
                          </div>
                        </div>
                        <div class="col-md-4 ccol-xs-12">
                          <div class="form-group">
                            <label> Brand</label>
                          <select class="form-control" name="brand_id">
                         <option value="">Please select a brand </option>
                        @foreach (App\Brand::orderBy('name', 'asc')->get() as $brand)
                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                       @endforeach
                      </select>
                          </div>
                          </div>
                           <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                              <label> Product Code</label>
                              <input type="text" class="form-control"  name="product_code"    id="productcode">
                            </div>
                             <input type="hidden" name="autovalue" value="{{ mt_rand(100000,999999) }}" id="autovalue">
                          </div>
                          <div class="col-md-4 col-xs-12">
                             <div class="form-group">
                               <label>Buy Price</label>
                                <input type="number" class="form-control" name="buy_price" value="0" required="">
                              </div>
                           </div>
                          <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Sell Price</label>
                             <input type="number" class="form-control" name="sell_price" required="" value="0">
                            </div>
                          </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Vat and Tax</label>
                            <input type="number" class="form-control"  name="vat" value="0">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          
                          <div class="form-group">
                            <label>Low Stok</label>
                            <input type="number" class="form-control"  name="low_stock" value="0">
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          
                          <div class="form-group">
                            <label>Up Stok</label>
                            <input type="text" class="form-control"  name="up_stock" value="0">
                          </div>
                        </div>
                        
                      </div>
                      <div class="row">
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                              <label>Unit</label>
                              <select class="form-control" name="unit_id">
                              @foreach (App\Unit::orderBy('name', 'asc')->get() as $unit)
                                <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                         <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Upload Photo</label>
                            <input type="file" class="form-control"  name="image" >
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                          <div class="form-group">
                            <label>Status</label>
                            <select class="form-control"  name="status">
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                            </select>
                          </div>
                        </div>
                         
                      </div> 
                      <button type="submit" class="btn btn-primary">Add Product</button>
                    </form>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          
        </div>
        </div> <!--/ panel body -->
        </div><!--/ panel -->
      </section>
    </div><!--/middle content wrapper-->
  </div><!--/ content wrapper -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script>
            function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) 
                {
                $('#show')
                  .attr('src', e.target.result)
                  .width(80)
                  .height(80);
                       };
            reader.readAsDataURL(input.files[0]);
            }
          }
        </script>

    <script type="text/javascript">
      $('#productcode').val( $('#autovalue').val()); // Hide the text input box in default
        function myFunction() {
           if($('#havecode').prop('checked')) {
                 $('#productcode').val('');
               } else {
                 $('#productcode').val($('#autovalue').val());
               }
        }
    </script>
@endsection