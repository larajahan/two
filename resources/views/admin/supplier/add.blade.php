@extends('admin.admin_layouts')
@section('admin_content')

<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Add Supplier</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							
							<div class="card">
								<h5 class="card-header">Supplier Form</h5>
								<div class="card-body">
									@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{ url('/admin/insert/supplier') }}" enctype="multipart/form-data" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Supplier Name</label>
													<input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" value="">
													@error('name')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 ccol-xs-12">
												<div class="form-group">
													<label>Email</label>
													<input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="">
													@error('email')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Address</label>
													<input type="text" class="form-control @error('address') is-invalid @enderror"  name="address" value="">
													@error('address')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Mobile</label>
													<input type="text" class="form-control @error('mobile') is-invalid @enderror"  name="mobile" value="">
													@error('mobile')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Position</label>
													<input type="text" class="form-control @error('position') is-invalid @enderror"  name="position" value="">
													@error('position')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Fax</label>
													<input type="text" class="form-control @error('fax') is-invalid @enderror"  name="fax" value="">
													@error('fax')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Contact Person</label>
													<input type="text" class="form-control @error('contact_person') is-invalid @enderror"  name="contact_person" value="">
													@error('contact_person')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Mobile Company</label>
													<input type="text" class="form-control @error('mobile_company') is-invalid @enderror"  name="mobile_company" value="">
													@error('mobile_company')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Bank Name</label>
													<input type="text" class="form-control @error('bank_name') is-invalid @enderror"  name="bank_name" value="">
													@error('bank_name')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 ccol-xs-12">
												<div class="form-group">
													<label>Account Name</label>
													<input type="text" class="form-control @error('account_name') is-invalid @enderror" name="account_name" value="">
													@error('account_name')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Account NO</label>
													<input type="text" class="form-control @error('account_number') is-invalid @enderror"  name="account_number" value="">
													@error('account_number')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-3 col-xs-12">
												<div class="form-group">
													<label>Brance Name</label>
													<input type="text" class="form-control @error('branch') is-invalid @enderror"  name="branch" value="">
													@error('branch')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
										</div>
										<div class="form-row">
											
											<div class="col-md-4 ccol-xs-12">
												<div class="form-group">
													<label>Opening Blance</label>
													<input type="text" class="form-control @error('opening_balance') is-invalid @enderror" name="opening_balance" value="0">
													@error('opening_balance')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Upload Photo</label>
													<input type="file" class="form-control @error('image') is-invalid @enderror"  name="image" >
													@error('image')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
													@enderror
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-primary">Add Supplier</button>
									</form>
								</div>
							</div>
							
							
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
				<!--/ page content -->
				<!-- start code here... -->
				</div><!--/middle content wrapper-->
				</div><!--/ content wrapper -->
				@endsection