@extends('admin.admin_layouts')
@section('admin_content')
    <script
            src="https://code.jquery.com/jquery-3.4.1.slim.js"
            integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
            crossorigin="anonymous"></script>


    <div class="content_wrapper">

        <div class="middle_content_wrapper">
            <section class="page_content">
                <div class="panel mb-0">
                    <div class="panel_header">
                        <div class="panel_title">
                            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span> Employee Salary</span>
                        </div>
                    </div>
                    <div class="panel_body">


                            <form action="" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleSelect1">Select Branch</label>
                                            <select id="branch" name="branch" class="form-control" id="branch">
                                                <option selected disabled>Select</option>
                                                @foreach($branchs as $branch)

                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                @endforeach
                                                <option value="master">Master</option>
                                                <option value="all">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleSelect1">Select Employee</label>
                                            <select name="employee" class="form-control" id="employee">
                                                <option value="0">select</option>
                                            </select>
                                        </div>
                                    </div>



                                    {{--<button type="submit" class="btn btn-success">Show</button>--}}

                                </div>

                            </form>

                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <div class="card">
                                    <div class="card-header">Eployee Details</div>
                                    <div class="card-body" >
                                        <div class="row">


                                            <div class="col-md-6">
                                                <div class="card" style="width:400px">
                                                    <span id="e_img"></span>

                                                    <div class="card-body">
                                                        <h4 id="e_name" class="card-title"> </h4>
                                                        <p id="e_mobile"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card" style="width:400px">


                                                    <div class="card-body">
                                                        <p id="e_bank"></p>
                                                        <p id="e_banka"></p>
                                                        <p id="e_salary"></p>
                                                        <p id="e_perday"></p>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>



                                </div>


                            </div>
                        </div>


                    </div>
                </div> <!--/ panel body -->


            </section>
            <!--/ page content -->
            <!-- start code here... -->

        </div><!--/middle content wrapper-->
    </div><!--/ content wrapper -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="branch"]').on('change', function(){
                var branch_id = $(this).val();
                if(branch_id) {
                    $.ajax({
                        url: "{{  url('/get/employee/') }}/"+branch_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data) {
                            $('#employee').empty();
                            $('#employee').append(' <option value="0">--Select--</option>');
                            $.each(data,function(index,empObj){
                                $('#employee').append('<option value="' + empObj.id + '">'+empObj.name+'</option>');
                            });
                        }
                    });
                } else {
                    alert('danger');
                }
            });
        });
        </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="employee"]').on('change', function(){
                var employee_id = $(this).val();
                if(employee_id) {
                    $.ajax({
                        url: "{{  url('/get/employeeinfo/') }}/"+employee_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data) {
                            console.log(data.attendances)

                            $('#e_name').empty();
                            $('#e_name').append('Name: ' + data.name);

                            $('#e_banka').empty();
                            $('#e_banka').append('Account Number: ' + data.account_number);

                            $('#e_bank').empty();
                            $('#e_bank').append('Bank Name: ' + data.bank_name);

                            $('#e_salary').empty();
                            $('#e_salary').append('Salary: ' + data.salary);

                            $('#e_perday').empty();
                            $('#e_perday').append('Perday: ' + data.perday);

                            $('#e_mobile').empty();
                            $('#e_mobile').append('Mobile: ' + data.mobile);

                            $('#e_img').empty();
                            $('#e_img').append('<img  class="card-img-top" src="{{url('')}}/public/panel/employee/'+data.image+ ' " alt="Card image" style="width:100%">');



                            // $.each(data,function(index,eObj){
                            //     $('#e_name').append('<h4 class="card-title">eObj.name</h4>');
                            // });
                        }
                    });
                } else {
                    alert('danger');
                }
            });
        });
    </script>


@endsection

