@extends('admin.admin_layouts')
@section('admin_content')


<div class="content_wrapper">

  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>View Employee</span>
          </div>
        </div>
        <div class="panel_body">

            <div class="row">
                <div class="col-md-6 border-right">
                    <p><strong>Employee ID : </strong>{{$employee->id}}</p>
                    <p><strong>Employee ID : </strong>{{$employee->employee_id}}</p>
                    <p><strong> Name : </strong>{{$employee->name}}</p>
                    <p><strong> Email : </strong>{{$employee->email}}</p>
                    <p><strong> Address : </strong>{{$employee->address}}</p>
                    <p><strong> Mobile : </strong>{{$employee->mobile}}</p>
                    <p><strong> Fax : </strong>{{$employee->fax}}</p>
                    <p><strong> Contact : </strong>{{$employee->contact_person}}</p>
                    <p><strong> Position : </strong>{{$employee->position}}</p>
                </div>
                <div class="col-md-6">

                    <p><strong> Company Person: </strong> {{$employee->company_person}}</p>
                    <p><strong> Bank  Name: </strong> {{$employee->bank_name}}</p>
                    <p><strong> Account Number : </strong> {{$employee->account_number}}</p>
                    <p><strong>  Account Name: </strong> {{$employee->account_name}}</p>
                    <p><strong> Branch: </strong> {{$employee->branch}}</p>
                    <p><strong> Salary  Month: </strong> {{$employee->salary}}</p>
                    <p><strong> Salary Day: </strong> {{$employee->perday}}</p>
                    <img id="logo" src="{{asset('public/panel/employee/'.$employee->image) }}" width="100" height="100;" />

                    <div>
                        <a href="{{route('admin.employee.index')}}" class="btn btn-sm btn-primary">Back</a>

                    </div>

                </div>
            </div>


          </div>
          </div> <!--/ panel body -->


        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->


@endsection
