<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- content wrpper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img class="img-fluid rounded w-50" src="http://via.placeholder.com/400x90?text=logo">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #550</p>
                            <p class="text-muted">Due to: 4 Dec, 2019</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Supplier Information</p>
                                        
                                           <p class="mb-1">Supplier ID:{{$supplierid->supplier_id}} </p>
                                           <p class="mb-1">Supplier Name:{{$supplierid->name}} </p>
                                            <p class="mb-1">Phone No:{{$supplierid->mobile}} </p>
                                          <p class="mb-1">Email:{{$supplierid->email}} </p>
                                            <p class="mb-1">Address:{{$supplierid->address}} </p>
                                           <p class="mb-1">Position:{{$supplierid->position}} </p>
                                      <p class="mb-1">Bank Name:{{$supplierid->bank_name}} </p>
                                          <p class="mb-1">Account No:{{$supplierid->account_number}} </p>
                                            <p class="mb-1">Account Name:{{$supplierid->account_name}}</p>
                                       
                           
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-4">Payment Details</p>
                            <p class="mb-1"><span class="text-muted">VAT: </span> 1425782</p>
                            <p class="mb-1"><span class="text-muted">VAT ID: </span> 10253642</p>
                            <p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
                            <p class="mb-1"><span class="text-muted">Name: </span> John Doe</p>
                        </div>
                    </div>


                     @php 
               $content=Cart::content();

                 @endphp
                  <h3 align="center">Product Details</h3>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">Item Code</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Product Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Unit</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Rate</th>
                                        <th class="border-0 text-uppercase small font-weight-bold"> QTY</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                     @foreach($content as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td >{{$row->name }}</td>             
                                        <td >{{$row->weight }}</td>
                                         <td>{{$row->price}}</td>
                                         <td>{{ $row->qty }}</td>
                                         <td>{{ $row->subtotal }} Taka</td>
                                    </tr>
                                      @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-white text-dark p-4">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light"> @php 
                     $price = '1,000';
                     $price2 =str_replace(",", "", Cart::subtotal());

                     echo (int)$price2;
                    @endphp">: Taka</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Discount</div>
                            <div class="h2 font-weight-light">10%</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Sub - Total amount</div>
                            <div class="h2 font-weight-light">$32,432</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://totoprayogo.com">totoprayogo.com</a></div>

</div>


<script type="text/javascript">
    
    function printDoc(){
        window.print();
    }
    document.getElementById("btn").addEventListener("click",printDoc);
</script>
<script type="text/javascript">

$(document).ready(function() {
$('select[name="category_id"]').on('change', function(){
var productid = $(this).val();
if(productid) {
$.ajax({
url: "{{  url('/get/buyer/') }}/"+productid,
type:"GET",
dataType:"json",
success:function(data) {

$('#product_name').empty();
$('#product_name').append(' <option value="">--Select--</option>');
$.each(data,function(index,data){
$('#product_name').append('<option value="' + data.id + '">'+data.product_name+'</option>');
});
},

});
} else {
alert('danger');
}
});
});
</script>

<script type="text/javascript">

$(document).ready(function() {
$('select[name="supplier_name"]').on('change', function(){
var supplierid = $(this).val();
if(supplierid) {
$.ajax({
url: "{{  url('/get/supplier/') }}/"+supplierid,
type:"GET",
dataType:"json",
success:function(data) {

$('#supplierid').empty();
$('#supplierid').val(data.supplier_id);
$('#mobile').empty();
$('#mobile').val(data.mobile);
$('#address').empty();
$('#address').val(data.address);

},
});
};
});
});
</script>
<script type="text/javascript">

$(document).ready(function() {
$('select[name="product_name"]').on('change', function(){
var productid = $(this).val();
if(productid) {
$.ajax({
url: "{{  url('/get/product/') }}/"+productid,
type:"GET",
dataType:"json",
success:function(data) {

$('#productid').empty();
$('#productid').val(data.product_code);
$('#product_code').empty();
$('#product_code').val(data.product_code);
$('#buy_price').empty();
$('#buy_price').val(data.buy_price);
$('#rate').empty();
$('#rate').val(data.buy_price);
},
});
};
});
});
</script>
<script type="text/javascript">
function sum() {
var qty = document.getElementById('qty').value;
var rate = document.getElementById('rate').value;
var result = parseInt(qty) * parseInt(rate) ;
if (!isNaN(result)) {
$('#amount').empty();
document.getElementById('amount').value = parseInt(result);
}
}
</script>
<script type="text/javascript">
function add() {
var total = document.getElementById('total').value;
var dis = document.getElementById('dis').value;
var result = parseInt(total) - parseInt(dis);
if (!isNaN(result)) {
$('#payable').empty();
document.getElementById('payable').value = parseInt(result);
}
}
</script>
<script type="text/javascript">
function done() {
var payable = document.getElementById('payable').value;
var cash = document.getElementById('cash').value;
var result = parseInt(payable) - parseInt(cash);
if (!isNaN(result)) {
$('#due').empty();
document.getElementById('due').value = parseInt(result);
}
}
</script>

