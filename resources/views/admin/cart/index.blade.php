@extends('admin.admin_layouts')

@section('admin_content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- content wrpper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>


<div class="content_wrapper">
  <div class="middle_content_wrapper">
    <section class="page_content">
    <div class="panel mb-0">
     <div class="inventory_ms mt-2">
    <form action="{{route('admin.cart.store')}}" method="post">
      @csrf
      <div class="row">

        <div class="col-md-3 border py-2">
          <div class="form-group row">
           @php 
          $category=DB::table('categories')->get();
          @endphp
            <div class="col-md-4">
              <span>Category</span>
            </div>
            <div class="col-md-8">
             <select class="form-control" name="category_id" id="category_id">
             <option selected disabled="">Select</option>
                @foreach($category as $row)
                <option value="{{ $row->id }}">{{ $row->name }}</option>
                  @endforeach
            </select>
            </div>
          </div>
            </div>

            <div class="form-group row">
            <div class="col-md-4">
              <span>Supplier Name</span>
            </div>
            <div class="col-md-8">
           <select  name="supplier_id" id="supplier_name" onchange="myFunction()" class="form-control">
             <option selected="" disabled="">==choose one==</option>
             @foreach (App\Supplier::orderBy('name', 'asc')->get() as $supp)
               <option value="{{ $supp->id }}">{{ $supp->name }}</option>
                @endforeach
            </select>
            </div>
          </div>

          </div>
        </div>
      </div>
      <div class="form-row">

    <div class="col-md-3 col-xs-12">
        <div class="form-group">
          <label>Item Code</label>
          <input type="text" class="form-control"  name="item_code" required="" id="product_code" readonly="">
        </div>
      </div>

      <div class="col-md-3 col-xs-12">
        <div class="form-group">
        <label>Item Name</label>
        
        <select class="form-control " name="product_name" id="product_name" value="" >
       </select>
        </div>
      </div>

     <div class="col-md-3 col-xs-12">
      <div class="form-group">
        <label>Rate</label>
        <input type="text" class="form-control"  name="rate"  id="rate"  readonly="" onkeyup="sum();">
      </div>
    </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label>MRP</label>
            <input type="text" class="form-control"  name="mrp" id="buy_price">
          </div>
        </div>

         <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label>Unit</label>
          <select class="form-control" name="unit_id">
         @foreach (App\Unit::orderBy('name', 'asc')->get() as $unit)
        <option value="{{ $unit->id }}">{{ $unit->name }}</option>
           @endforeach
        </select>
          </div>
        </div>

         <div class="col-md-3 col-xs-12">
          <div class="form-group">
           <label>QTY</label>
            <input type="number" class="form-control"  name="qty" id="qty" onkeyup="sum();">
          </div>
        </div>

     <div class="col-md-3 col-xs-12">
      <div class="form-group">
        <label>Amount</label>
        <input type="text" class="form-control"  name="amount" id="amount" readonly="" >
      </div>
    </div>
    <div class="col-md-3 col-xs-12 pt-1">
      <div class="form-group pt-4">
       <button type="submit" class="btn btn-success float-left mt-2">Add </button>
      </div>
    </div>
    </div> 
      </form>
      <hr><hr>
@php 
 $content=Cart::content();

@endphp


     <h2 align="center">Product Details</h2>
    <div class="row mt-2">
        <div class="col-md-12">
          <table class="table table-bordered">
              <thead>
                <tr>     
                  <th>Item Code</th>
                  <th>Item Name</th>
                  <th>Unit</th>
                   <th>Rate</th>
                  <th>QTY</th>
                  <th>Amount</th>  
                  <td>Action</td>  
                </tr>
              </thead>
              <tbody>

                @foreach($content as $row)
                <tr>           
                 <td >{{$row->id}}</td>             
                 <td >{{$row->name }}</td>             
                 <td >{{$row->weight }}</td>
                 <td >{{$row->price}}</td>
                 <td >
                <form class="form-inline" action="{{route('cart.update',$row->rowId)}}" method="post">
                   @csrf
                   <input type="hidden" name="rowId" value="{{ $row->rowId }}" >
                  <input type="number" name="qty" value="{{ $row->qty }}" height="10", weight="20" / >
                  <button type="submit" class="btn btn-sm btn-success"><i class="fas fa-plus"></i></button>
                </form> 

               {{--   @php
                  $total_price += $buyer->rate * $buyer->qty;
                  @endphp --}}
                <td>
                {{ $row->subtotal }} Taka
                 </td>                                                        
                 <td>
                 <a class="btn btn-sm btn-danger" href="{{route('cart.delete',$row->rowId)}}" id="delete">Delete</a>
                 </td>              
                </tr>
                @endforeach
              </tbody>       
            </table>
                  
          </div>
          </div>
           <hr><hr>
       <h3 align="center">Product informationn</h3>
          <div class="row mt-2">
            <div class="col-md-6">
              <div class="row form-group">
                <div class="col-5">
                  <span>Number of items:</span>
                </div>
                  
                <div class="col-7">
                  <input type="text" class="form-control" name="count" placeholder="{{Cart::content()->count()}}">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Total QTY:</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="{{Cart::count()}}">
                </div>
              </div>
            
            </div>

            <div class="col-md-6">
              <div class="row form-group">
                <div class="col-5">
                  <span>Total</span>
                </div> 
             
                <div class="col-7">
                  <input type="text" class="form-control" id="total" name="total_amount" value="@php 
                     $price = '1,000';
                     $price2 =str_replace(",", "", Cart::subtotal());

                     echo (int)$price2;
                    @endphp">
                  
             </div>
              </div>
    
              <div class="row form-group">
                <div class="col-5">
                  <span>Dis, Amount  </span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control"   name="dis" id="dis" onkeyup="add();"  value=""> 
                </div>
              </div>

              <div class="row form-group">
                <div class="col-5">
                  <span>Net Total</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" name="payable" id="payable" value="">
                </div>
              </div>
               <div class="row form-group">
                <div class="col-5">
                  <span>Cash</span>
                </div>
                <div class="col-7">
                <input type="text" class="form-control" name="cash" id="cash" onkeyup="done();" >
                </div>
              </div>
              <div class="row form-group">
                <div class="col-5">
                  <span>Due</span>
                </div>
                <div class="col-7">
                  <input type="text" class="form-control" placeholder="0" id="due">
                </div>
              </div>
            </div>
          </div>
       
          <!-- ============================== Short button ============================ -->
            <div class="justify-content-center row">
             <a href="#" class="btn btn-success m-1">Save</a>
              <form action="{{route('admin.cart.invoice')}}" method="post">
               @csrf
               <input type="hidden" name="supplier_id" value="{{ $supp->id }}">
               <input type="hidden" name="qty" value="{{ $row->qty }}">
               <input type="hidden" name="item_code" value="{{$row->item_code}}">
               <input type="hidden" name="product_name" value="{{$row->product_name}}">    
               <input type="hidden" name="rate" value="{{$row->rate}}">
               <input type="hidden" name="amount" value="{{$row->amount}}">
               <input type="hidden" name="invoice" value="{{$row->invoice}}">
               <button type="submit" class="btn btn-primary m-1" >Print</button> 

             </form> 
                      
            </div>

       
          </div>
      </div>
    </section>
  </div>
</div>

<script type="text/javascript">
    
     $(document).ready(function() {
     $('select[name="category_id"]').on('change', function(){
     var productid = $(this).val();
     if(productid) {
       $.ajax({
     url: "{{  url('/get/buyer/') }}/"+productid,
    type:"GET",
    dataType:"json",
    success:function(data) {
          
    $('#product_name').empty();
    $('#product_name').append(' <option value="">--Select--</option>');
    $.each(data,function(index,data){
    $('#product_name').append('<option value="' + data.id + '">'+data.product_name+'</option>');
    });
    },
    
    });
    } else {
    alert('danger');
    }
    });
      });
    </script>



  
   <script type="text/javascript">
  
   $(document).ready(function() {
    $('select[name="supplier_name"]').on('change', function(){
    var supplierid = $(this).val();
    if(supplierid) {
    $.ajax({  
    url: "{{  url('/get/supplier/') }}/"+supplierid,
    type:"GET",
    dataType:"json",
    success:function(data) {
      
            $('#supplierid').empty();
                $('#supplierid').val(data.supplier_id);

                $('#mobile').empty();
                $('#mobile').val(data.mobile);

                $('#address').empty();
                $('#address').val(data.address);
                
        },
      });
    };
   });
   });    

 </script>

 <script type="text/javascript">
  
   $(document).ready(function() {
    $('select[name="product_name"]').on('change', function(){
    var productid = $(this).val();
    if(productid) {
    $.ajax({  
    url: "{{  url('/get/product/') }}/"+productid,
    type:"GET",
    dataType:"json",
    success:function(data) {
      
             $('#productid').empty();
                 $('#productid').val(data.product_code);
                $('#product_code').empty();
                $('#product_code').val(data.product_code);
                $('#buy_price').empty();
                $('#buy_price').val(data.buy_price);
                 $('#rate').empty();
                $('#rate').val(data.buy_price);       
        },
      });
    };
   });
   });    

 </script>
  <script type="text/javascript">
           function sum() {
               var qty = document.getElementById('qty').value;
               var rate = document.getElementById('rate').value;
                var result = parseInt(qty) * parseInt(rate) ;
               if (!isNaN(result)) {
                $('#amount').empty();
               document.getElementById('amount').value = parseInt(result);
               }
              }         
 </script>

 <script type="text/javascript">
     function add() {
         var total = document.getElementById('total').value;
         var dis = document.getElementById('dis').value;
          var result = parseInt(total) - parseInt(dis);
         if (!isNaN(result)) {
           $('#payable').empty();
         document.getElementById('payable').value = parseInt(result);
         }
        }
       </script>

<script type="text/javascript">
 function done() {
     var payable = document.getElementById('payable').value;
     var cash = document.getElementById('cash').value;
      var result = parseInt(payable) - parseInt(cash);
     if (!isNaN(result)) {
       $('#due').empty();
     document.getElementById('due').value = parseInt(result);
     }
    }
   </script>
        
@endsection
