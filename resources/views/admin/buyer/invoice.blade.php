@extends('admin.admin_layouts')
@section('admin_content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- content wrpper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
<section class="invoice_area">
	<div class="panel mb-0">
		<div class="panel_header">
			<div class="panel_title">
				<span>Invoice:</span>
			</div>
		</div>
		<div class="panel_body print_element">
			<div class="logo bg-blue">
				<img src="assets/images/logo.png" class="img-fluid" alt="">
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h3>Supplier Information</h3>
					<br>
					<address class="fs-13">
						<strong>Supplier ID:{{$supplierid->supplier_id}} </strong><br>
						<strong>Supplier Name:{{$supplierid->name}} </strong><br>
						<strong>Phone No:{{$supplierid->mobile}} </strong><br>
						<strong>Email:{{$supplierid->email}} </strong><br>
						<strong>Address:{{$supplierid->address}} </strong><br>
						<strong>Position:{{$supplierid->position}} </strong><br>
						<strong>Bank Name:{{$supplierid->bank_name}} </strong><br>
						<strong>Account No:{{$supplierid->account_number}} </strong><br>
						<strong>Account Name:{{$supplierid->account_name}} </strong>
					</address>
					
				</div>
				<div class="col-sm-6 item_right text-right">
					<h3 class="mt-0">Invoice #{{$invoice}}</h3>
					<div class="fs-13">Issued {{$date}}</div>
			</div>
		</div>
		<hr>

		  <h2 align="center">Product Details</h2>
    <div class="row mt-2">
        <div class="col-md-12">
          <table class="table table-bordered">
              <thead>
                <tr>     
                  <th>Item Code</th>
                  <th>Item Name</th>
                   <th>Rate</th>
                  <th>QTY</th>
                  <th>Amount</th> 
                </tr>
              </thead>
              <tbody>
                 <tr>
                 	<td>{{$item_code}}</td>
                 	<td>{{$product_name}}</td>
                 	<td>{{$rate}}</td>
                 	<td>{{$qty}}</td>
                 	<td>{{$amount}}</td>
                 </tr>
              </tbody>       
            </table>
                  
          </div>
          </div>
		<h2 align="center">Product Details</h2>
		<div class="row mt-2">
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-5">
						<span>Number of items:</span>
					   
					{{$count}}
					</div>
				</div>
				<div class="row form-group">
					<div class="col-5">
						<span>Total QTY:</span>
							{{$qty}}
					</div>
					
				
					
					
				</div>
				<div class="row form-group">
					<div class="col-5">
						<span>Rmarks:</span>
					</div>
					<div class="col-7">
						
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-5">
						<span>Total</span>
					</div>
					<div class="col-7">
						<input type="text" class="form-control" id="total" name="total" value="{{$total_amount}}">
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-5">
						<span>Dis, Amount</span>
					</div>
					<div class="col-7">
						<input type="text" class="form-control" id="dis" onkeyup="add();" >
					</div>
				</div>
				<div class="row form-group">
					<div class="col-5">
						<span>Net Total</span>
					</div>
					<div class="col-7">
						<input type="text" class="form-control" name="payable" id="payable">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-5">
						<span>Cash</span>
					</div>
					<div class="col-7">
						<input type="text" class="form-control" name="cash" id="cash" onkeyup="done();" >
					</div>
				</div>
				<div class="row form-group">
					<div class="col-5">
						<span>Due</span>
					</div>
					<div class="col-7">
						<input type="text" class="form-control" placeholder="0" id="due">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel_footer text-right">
	<a type="button" id="btn" class="btn btn-blue">Print</a> 
	
	</div>
</div>
</section>

<script type="text/javascript">
	
	function printDoc(){
		window.print();
	}
	document.getElementById("btn").addEventListener("click",printDoc);
</script>
<script type="text/javascript">

$(document).ready(function() {
$('select[name="category_id"]').on('change', function(){
var productid = $(this).val();
if(productid) {
$.ajax({
url: "{{  url('/get/buyer/') }}/"+productid,
type:"GET",
dataType:"json",
success:function(data) {

$('#product_name').empty();
$('#product_name').append(' <option value="">--Select--</option>');
$.each(data,function(index,data){
$('#product_name').append('<option value="' + data.id + '">'+data.product_name+'</option>');
});
},

});
} else {
alert('danger');
}
});
});
</script>

<script type="text/javascript">

$(document).ready(function() {
$('select[name="supplier_name"]').on('change', function(){
var supplierid = $(this).val();
if(supplierid) {
$.ajax({
url: "{{  url('/get/supplier/') }}/"+supplierid,
type:"GET",
dataType:"json",
success:function(data) {

$('#supplierid').empty();
$('#supplierid').val(data.supplier_id);
$('#mobile').empty();
$('#mobile').val(data.mobile);
$('#address').empty();
$('#address').val(data.address);

},
});
};
});
});
</script>
<script type="text/javascript">

$(document).ready(function() {
$('select[name="product_name"]').on('change', function(){
var productid = $(this).val();
if(productid) {
$.ajax({
url: "{{  url('/get/product/') }}/"+productid,
type:"GET",
dataType:"json",
success:function(data) {

$('#productid').empty();
$('#productid').val(data.product_code);
$('#product_code').empty();
$('#product_code').val(data.product_code);
$('#buy_price').empty();
$('#buy_price').val(data.buy_price);
$('#rate').empty();
$('#rate').val(data.buy_price);
},
});
};
});
});
</script>
<script type="text/javascript">
function sum() {
var qty = document.getElementById('qty').value;
var rate = document.getElementById('rate').value;
var result = parseInt(qty) * parseInt(rate) ;
if (!isNaN(result)) {
$('#amount').empty();
document.getElementById('amount').value = parseInt(result);
}
}
</script>
<script type="text/javascript">
function add() {
var total = document.getElementById('total').value;
var dis = document.getElementById('dis').value;
var result = parseInt(total) - parseInt(dis);
if (!isNaN(result)) {
$('#payable').empty();
document.getElementById('payable').value = parseInt(result);
}
}
</script>
<script type="text/javascript">
function done() {
var payable = document.getElementById('payable').value;
var cash = document.getElementById('cash').value;
var result = parseInt(payable) - parseInt(cash);
if (!isNaN(result)) {
$('#due').empty();
document.getElementById('due').value = parseInt(result);
}
}
</script>



@endsection