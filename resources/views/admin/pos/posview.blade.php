@extends('admin.pos.pos')
@section('admin_content')
<style>
	
	
	.pos_content_wrapper{
		position: fixed;
		width: 100%;
		top:0px;
		bottom:0px;
		right:0px;
		left:0px;
		padding: 80px 0px;
	}
	.pos_content_wrapper .pos_body_content{
		width: 100%;
		max-height:100%;
		overflow: hidden;
		overflow-y: scroll;
	}
	.pos_content_wrapper .pos_body_content .left_side{
		width:60%;
		height:100%;
		overflow-y: scroll;
		overflow-x: hidden;
		float:left;
		padding:10px 20px 10px 10px;
	}
	..pos_content_wrapper.header_content{
		width: 100%;
		max-height:100px;
		
		}
	
	.pos_content_wrapper .pos_body_content .right_side{
		position: relative;
		display: block;
		overflow: hidden;
		width: 40%;
		height:100%;
		float:left;
	}
	.pos_content_wrapper .pos_body_content .top_table{
		height:70%;
		width:100%;
		overflow-y: scroll;
	}
	.pos_content_wrapper .pos_body_content .table_fixed{
		position: absolute;
		width:100%;
		height:30%;
		bottom: 0px;
		right:0px;
		left:0px;
	}
	.pos_content_wrapper .footer_area_fixed{
		width:100%;
		position: absolute;
		bottom: 0px;
		right:0px;
		left:0px;
	}
	.form-rounded {
border-radius: 1rem;
}
.card_iamge{
	height: 80px;
	width: 80px;
	margin:0 auto;
	padding: 0;
}
.card_product{
	position: relative;
	float: left;
}
	@media only screen and (max-width: 991px){
		.pos_content_wrapper .pos_body_content .left_side,
		.pos_content_wrapper .pos_body_content .right_side{
			width:100%;
		}
		
	}
	
</style>
<section class="middle_section  pos_content_wrapper">
	<div class="header_content">
		<div class="row">
			<div class="col-7">
				<div class="left_header mb-1">
					<div class="row">
						
						<div class="col-7 ">
							<div class="input-group ml-3">
								<div class="input-group-append" title="Search Product">
									<button class="btn btn-outline-secondary" type="button">
								<i class="fas fa-search"></i>
									</button>
								</div>
								<input class="form-control border-secondary " type="search" value="Search Product">
							</div>
						</div>
						<div class="col-5 ">
							<div class="input-group ">
								
								<select class="custom-select" id="inputGroupSelect01">
									<option selected>Choose...</option>
									<option value="1">One</option>
									<option value="2">Two</option>
									<option value="3">Three</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-5">
				<div class="right_header">
				<div class="row">
					<div class="col-9">
							<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label title="Search Customer" class="input-group-text" for="inputGroupSelect01"><i class="fas fa-user-tie"></i></label>
						</div>
					<input class="form-control border-secondary " type="search" value="Search Customer">
						<div class="input-group-prepend">
							<label title="Edit Customer" class="input-group-text btn" for="inputGroupSelect01"><i class="fas fa-user-edit"></i></label>
						</div>
					</div>
					</div>
					
					<div class="col-3">  
                        <a href="#" title="Add Customer" class="btn btn-info btn-lg">
                            <i class="fas fa-plus"></i> 
                         </a>
                    </div>
				</div>


				</div>
			</div>
		</div>
	</div>
	<div class="pos_body_content">
		<div class="left_side">
			<div class="row">

				<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			<div class="col-sm-2 btn">
					<div class="card card_product text-center " >
						<img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/vans.png" class="card_iamge img-fluid img-thumbnai" alt="card image">
						<div class="card-body">
							<h5 class=" card-subtitle">Mens Shoe</h5>
							
						</div>
					</div>
				</div>
              
			
		
				


				
				
			</div>
		</div>
		
		
		<div class="right_side">
			<div class="bg-light top_table">
				<div class="table-responsive">
					<table class="table table-striped ">
						<thead class="postablecolor">
							<tr>
								<th scope="col">Quantity</th>
								<th scope="col">Product</th>
								<th scope="col">Price</th>
								<th scope="col">Subtotal</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>

							<tr>
								<td class="w-25"><input type="number" min="1" value="1" class="form-control form-rounded  w-50" ></td>
								<td>Larry</td>
								<td>10$</td>
								<td>500$</td>
								<td>
									<button type="button" class="btn btn-outline-danger btn-xs">
									<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
								</td>
							</tr>
							
							
							
							
							
						</tbody>
					</table>
				</div>
				
			</div>
			{{-- fixed table --}}
			<div class="table_fixed">
				<div class="table-responsive">
					<table class="table">
						<thead class="postablecolor">
							<tr>
								<th scope="col">TOTAL ITEM</th>
								<th scope="col">0 (0)	</th>
								<th scope="col">TOTAL</th>
								<th scope="col">0.00</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>DISCOUNT</td>
								<td><input type="text" value="0" class="form-control form-rounded w-75" ></td>
								<td>TAX AMOUNT (%)	</td>
								<td><input type="text" value="0" class="form-control  form-rounded w-75" ></td>
							</tr>
							<tr>
								<td>SHIPPING CHARGE</td>
								<td><input type="text" value="0" class="form-control  form-rounded w-75" ></td>
								<td>OTHER CHARGE	</td>
								<td><input type="text" value="0" class="form-control  form-rounded w-75" ></td>
							</tr>
							
						</tbody>
						<thead class="postablecolor">
							<tr>
								<th></th>
								<th></th>
								
								<th scope="col">TOTAL</th>
								<th scope="col">0.00</th>
							</tr>
						</thead>
					</table>
				</div>
				
			</div>
			{{-- / fixed table --}}
			
		</div>
		{{-- / right side --}}
		
		
		<div class="footer_area_fixed row">
			<div class="col-4  bg-warning">
				<h2 class="font-weight-bold text-uppercase text-white text-center mt-3 mb-4">
				0.00 <small>Tk.</small>
				</h2>
			</div>
			<div class="col-4  bg-success">
				
				<a  href="#">
					<h4 class="font-weight-bold text-uppercase  text-white text-center mt-4 mb-4"> <i class="fas fa-money-bill-alt"></i>  PAY NOW
					</h4>
				</a>
				
			</div>
			<div class="col-4  bg-danger">
				<a  href="#">
					<h4 class="font-weight-bold text-uppercase  text-white text-center mt-4 mb-4"> <i class="fas fa-crosshairs"></i> HOLD
					</h4>
				</a>
			</div>
		</div>
	</div>
	{{--/ content body --}}
</section>
@endsection
<script>
	$(document).ready(function(){
// jQuery methods go here...
});
</script>