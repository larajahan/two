@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Deposits</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="table-responsive">
            <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
              <thead>
                <tr>
                  <th>Invoice Name</th>
                  <th>Date</th>
                  <th>Bank Name</th>
                  <th>Account Holder</th>
                  <th>Account Number</th>
                  <th>Address</th>
                  <th>Pay Mode</th>
                  <th>Send Bank</th>
                  <th>Send Account Holder</th>
                  <th>Check No / Transaction</th>
                  <th>Payment Amount</th>
                  <th>Remarks</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($deposits as $row)
                <tr>
                  <td >{{$row->invoice_no}}</td>
                  <td>{{$row->date}}</td>
                  <td>{{$row->bank_name}}</td>
                  <td>{{$row->sender_holder_name}}</td>
                  <td>{{$row->sender_account_no}}</td>
                  <td>{{$row->sender_account_address}}</td>
                  <td>{{$row->pay_mode}}</td>
                  <td>{{$row->recieve_bank}}</td>
                  <td>{{$row->whorecieve}}</td>
                  <td>{{$row->check_no}}</td>
                  <td>{{$row->payment_amount}}</td>
                  <td>{{$row->remarks}}</td>
                  <td>
                    
                    <div class="dropdown">
                      <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Action
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                           
                        <a class="dropdown-item" href="{{route('admin.single.deposits',$row->id)}}"  >View</a>
                        <a class="dropdown-item" href="{{route('admin.delete.deposits',$row->id)}}" id="delete"  >Delete</a>
                        <a class="dropdown-item" href="{{route('admin.edit.deposits',$row->id)}}"  >Edit</a>                        
                      </div>
                    </div>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
          
        </section>
        <!--/ page content -->
        <!-- start code here... -->
        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->
        @endsection