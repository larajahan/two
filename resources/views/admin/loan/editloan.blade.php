@extends('admin.admin_layouts')
@section('admin_content')
@php  
 $loanfroms=DB::table('loanfroms')->get(); 

@endphp
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header ">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span> Edit Loan</span>
					</div>
				</div>
				<div class="panel_body ">
					<div class="row">
						<div class="col-md-10 col-xs-12 offset-1">
							
							<div class="card">
								<h5 class="card-header">Loan Form</h5>
								<div class="card-body">
									<div class="modal-footer">									
										
										<a type="submit" class="btn btn-primary" href="{{url('admin/all/loan')}}">All Loan</a>
									</div>
									
									{{-- Loan Form --}}
								@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{route('admin.update.loan')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<input type="hidden" class="form-control"  name="id" value="{{ $loanedit->id}}" >
													<label>Date</label>
										<input type="date" class="form-control"  name="date" value="{{$loanedit->date}}">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Loan From</label>
													<select class="form-control" id="loan_from" name="loan_from">
														<option  selected disabled="">Select</option>
														@foreach($loanfroms as $row)
														<option value="{{$row->id}}"  <?php if ($row->id == $loanedit->loan_from) {
															echo "selected";
														} ?>>{{$row->loanfrom}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Title</label>
													<input type="text" name="title" value="{{$loanedit->title}}"   class="form-control">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Amount</label>
													<input type="text" class="form-control " value="{{$loanedit->amount}}"  name="amount"  id="amount"  >
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Interest</label>
													<input type="text" name="interest" value="{{$loanedit->interest}}"  class="form-control" id="interest" onkeyup="sum();">
												</div>
											</div>
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Payable</label>
													<input type="text" name="payable" value="{{$loanedit->payable}}"  class="form-control" readonly=""  id="payable">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="col-md-4 col-xs-12">
												<div class="form-group">
													<label>Remarks</label>
													<input type="text" name="remarks" value="{{$loanedit->remarks}}" class="form-control">
												</div>
											</div>
										</div>
										
										<button type="submit" class="btn btn-primary">Update</button>
									</form>
								</div>
							</div>
							
							
						</div>
					
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
			</div>
		</div>
	
			
		<script type="text/javascript">
           function sum() {
               var amount = document.getElementById('amount').value;
               var interest = document.getElementById('interest').value;
               var percentage = parseInt(amount)*(parseInt(interest)/100);
                var result = parseInt(amount) + percentage;
               if (!isNaN(result)) {
               	 $('#payable').empty();
               document.getElementById('payable').value = parseInt(result);
               }
              }
             </script>
		
		
		
		
		@endsection