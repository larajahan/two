@extends('admin.admin_layouts')
@section('admin_content')

<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->


      <div class="row"> 
        <div class="col-md-8 offset-1"> 
      <div class="panel mb-0">
        <div class="panel_header ">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Loan Details</span>
          </div>
        </div>
        <div class="panel_body  ">
          
          
          <div class="row">
            
            <div class="col-md-8 offset-2">
              <div class="user_info">
                <div class="table-responsive">
                  <table class="table table-hover mt-2">                  
                    <tbody>
                      
                      <tr>
                        <td class="font-weight-bold">Date</td>
                         <td>{{$single_view->date}}</td>
                      </tr>  
                                     
                       <tr>
                         <td class="font-weight-bold">Loan From</td>
                         <td>{{$single_view->loanfrom}}</td>
                      </tr>
                      <tr>
                         <td class="font-weight-bold">Title</td>
                         <td>{{$single_view->title}}</td>
                      </tr> 
                      <tr>
                         <td class="font-weight-bold">Amount</td>
                         <td>{{$single_view->amount}}</td>
                      </tr>
                       <tr>
                         <td class="font-weight-bold">Interst</td>
                         <td>{{$single_view->interest}}</td>
                      </tr> 
                      <tr>
                         <td class="font-weight-bold">Payable</td>
                         <td>{{$single_view->payable}}</td>
                      </tr>  
                      <tr>
                         <td class="font-weight-bold">Remarks</td>
                         <td>{{$single_view->remarks}}</td>
                      </tr>
                        <tr>                        
                         <td><a class="btn btn-info btn-sm" href="{{url('admin/all/loan')}}">Back</a></td> 
                          </tr>
                    </tbody>                  
                  </table>               
            </div>         
          </div>
          
          
          
         
          
          </div> <!--/ panel body -->
          </div><!--/ panel -->
          </div>
           </div>
        </section>
        <!--/ page content -->
        <!-- start code here... -->
        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->
        @endsection