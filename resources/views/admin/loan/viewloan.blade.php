@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Loans</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="table-responsive">
            <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
              <thead>
                <tr>          
                  <th>Date</th>
                  <th>Loan From</th>
                  <th>Title</th>
                  <th>Amount</th>                
                  <th>Interst</th>
                  <th>Payable</th>
                  <th>Remarks</th>                  
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($loans as $row)
                <tr>                
                  <td>{{$row->date}}</td>
                  <td>{{$row->loan_from}}</td>
                  <td>{{$row->title}}</td>
                  <td>{{$row->amount}}</td>                  
                  <td>{{$row->interest}}</td>
                  <td>{{$row->payable}}</td>
                  <td>{{$row->remarks}}</td>                 
                  <td>
                    
                    <div class="dropdown">
                      <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Action
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                           
                        <a class="dropdown-item" href="{{route('admin.single.loan',$row->id)}}"  >View</a>
                        <a class="dropdown-item" href="{{route('admin.delete.loan',$row->id)}}" id="delete"  >Delete</a>
                        <a class="dropdown-item" href="{{route('admin.edit.loan',$row->id)}}"  >Edit</a>                        
                      </div>
                    </div>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
          
        </section>
        <!--/ page content -->
        <!-- start code here... -->
        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->
        @endsection