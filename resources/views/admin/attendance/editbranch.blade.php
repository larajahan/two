@extends('admin.admin_layouts')
@section('admin_content')


  <div class="content_wrapper">

    <div class="middle_content_wrapper">
      <section class="page_content">
        <div class="panel mb-0">
          <div class="panel_header">
            <div class="panel_title">
              <span class="panel_icon"><i class="fas fa-border-all"></i></span><span> Employee Attendance</span>
            </div>
          </div>
          <div class="panel_body">

            <div class="row">
              <div class="col-md-6 offset-md-3">
                <form action="{{route('admin.edit.branch.save')}}" method="post">
                  @csrf
                  <div class="form-group">
                    <label for="exampleSelect1">Select Branch</label>
                    <select id="branch" name="branch" class="form-control" id="exampleSelect1">
                      @foreach($branchs as $branch)
                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                      @endforeach
                      <option value="master">Master</option>
                      <option value="all">All</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-success">Show</button>


                </form>

              </div>


            </div>


          </div>
        </div> <!--/ panel body -->


      </section>
      <!--/ page content -->
      <!-- start code here... -->

    </div><!--/middle content wrapper-->
  </div><!--/ content wrapper -->


@endsection

