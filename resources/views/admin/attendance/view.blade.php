@extends('admin.admin_layouts')
@section('admin_content')


<div class="content_wrapper">

  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>View Employee</span>
              <a class="btn btn-success float-right" href="{{route('admin.attendance.index')}}">Back</a>
          </div>
        </div>
        <div class="panel_body">

         <div class="row">
             <form action="{{route('admin.save.attendance')}}" method="post">
                 @csrf
             <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                 <thead>
                 <tr>
                     <th>Employee ID</th>
                     <th>Employee Name</th>
                     <th>Branch</th>
                     <th>Mobile</th>
                     <th>Image</th>
                     <th class="text-center" >Attendance</th>

                 </tr>
                 </thead>

                 <tbody>

                 @foreach($employees as $employee)
                     <tr>
                         <td >{{$employee->employee_id}}</td>
                         <td>{{$employee->name}}</td>
                         @if($employee->user_id =='master')
                             <td>Master</td>
                         @else
                         <td>{{$employee->branchss->name}}</td>
                         @endif
                         <td>{{$employee->mobile}}</td>
                         <td><img id="logo" src="{{asset('public/panel/employee/'.$employee->image) }}" width="50" height="50;" /></td>
                         <td>
                             <fieldset class="form-group">

                                 <div>
                                     <label class="form-check-label">
                                         <input type="radio" name="attendence[{{$employee->id}}]" id="optionsRadios1" value="present" checked="">
                                         Present &nbsp;&nbsp;
                                         <input type="radio" name="attendence[{{$employee->id}}]" id="optionsRadios1" value="absent">
                                         Absent &nbsp;&nbsp;
                                         <input type="radio" name="attendence[{{$employee->id}}]" id="optionsRadios1" value="vacation">
                                        Vacation
                                     </label>
                                 </div>

                             </fieldset>
                         </td>
                         <input type="hidden" name="branch" value="{{($employee->user_id =='master') ? 'master' : $employee->branchss->id}}">
                         <input type="hidden" name="attendance_year" value="{{ date("Y") }}">
                         <input type="hidden" name="attendance_month" value="{{ date("F") }}">
                         <input type="hidden" name="attendance_day" value="{{ date("l") }}">
                         <input type="hidden" name="date" value="{{ date("Y-m-d") }}">
                         <input type="hidden" name="emp_id[]" value="{{ $employee->id }}">




                     </tr>
                 @endforeach
                 </tbody>

             </table>
             <button type="submit" class="btn btn-success ">Save </button>
             </form>

         </div>

            
          </div>
          </div> <!--/ panel body -->


        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->


@endsection
