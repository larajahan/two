@extends('admin.admin_layouts')
@section('admin_content')


<div class="content_wrapper">

  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Update Employee</span>
              <a class="btn btn-success float-right" href="{{route('admin.attendance.index')}}">Back</a>
          </div>
        </div>
        <div class="panel_body">

         <div class="row">
             <form action="{{route('admin.attendance.today.branch.update')}}" method="post">
                 @csrf
             <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                 <thead>
                 <tr>
                     <th>Employee ID</th>
                     <th>Employee Name</th>
                     <th>Branch</th>
                     <th>Mobile</th>
                     <th>Image</th>
                     <th class="text-center" >Attendance</th>

                 </tr>
                 </thead>

                 <tbody>

                 @foreach($att_today as $att_t)
                     <tr>
                         <td >{{$att_t->employee->employee_id}}</td>
                         <td>{{$att_t->employee->name}}</td>
                         @if($att_t->branch =='master')
                             <td>Master</td>
                         @else
                             <td>{{$att_t->branchas->name}}</td>
                         @endif
                         <td>{{$att_t->employee->mobile}}</td>
                         <td><img id="logo" src="{{asset('public/panel/employee/'.$att_t->employee->image) }}" width="50" height="50;" /></td>

                         <td>
                             <fieldset class="form-group">

                                 <div>
                                     <label class="form-check-label">
                                         <input {{($att_t->attendance == 'present') ? 'checked' :''}} type="radio" name="attendence[{{$att_t->id}}]" id="optionsRadios1" value="present" >
                                         Present &nbsp;&nbsp;
                                         <input {{($att_t->attendance == 'absent') ? 'checked' :''}} type="radio" name="attendence[{{$att_t->id}}]" id="optionsRadios1" value="absent">
                                         Absent &nbsp;&nbsp;
                                         <input {{($att_t->attendance == 'vacation') ? 'checked' :''}} type="radio" name="attendence[{{$att_t->id}}]" id="optionsRadios1" value="vacation">
                                        Vacation
                                     </label>
                                 </div>

                             </fieldset>
                         </td>
                         <input type="hidden" name="branch[{{$att_t->id}}]" value="{{($att_t->branch =='master') ? 'master' : $att_t->branchas->id}}">
                         <input type="hidden" name="attendance_year" value="{{ date("Y") }}">
                         <input type="hidden" name="attendance_month" value="{{ date("F") }}">
                         <input type="hidden" name="attendance_day" value="{{ date("l") }}">
                         <input type="hidden" name="date" value="{{ date("Y-m-d") }}">
                         <input type="hidden" name="emp_id[{{$att_t->id}}]" value="{{$att_t->employee->id}}">
                         <input type="hidden" name="id[]" value="{{$att_t->id}}">




                     </tr>
                 @endforeach
                 </tbody>

             </table>
             <button type="submit" class="btn btn-success ">Update </button>
             </form>

         </div>

            
          </div>
          </div> <!--/ panel body -->


        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->


@endsection
