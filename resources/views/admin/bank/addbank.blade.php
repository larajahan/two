@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper">
		<section class="page_content ">
			<!-- Button trigger modal -->
			
			<!-- Modal -->
			<div class="modal fade mt-3" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" width="50">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Add Bank</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
								@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
							<form action="{{route('admin.insert.bank')}}" method="post">
								@csrf
								<div class="form-group">
									<label>Bank Name</label>
									<input type="text" class="form-control " name="bank_name" value="">
								</div>
								
								
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Add Bank</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- panel -->
			<!-- panel -->
			<div class="panel mb-0 w-75 offset-1">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">					
			Add Bank
			</button>

				<div class="panel_header mt-2">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Bank</span>
					</div>
				</div>
				<div class="panel_body">
					
					<div class="table-responsive">
						<table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
							<thead>
								<tr>
									<th>Bank Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($banks as $bank)
								<tr>
									<td >{{$bank->bank_name}}</td>
									<td>
										<div class="btn-group" role="group">
											
											<a href="{{route('admin.edit.bank',$bank->id)}}"  class="btn btn-sm btn-primary">Edit</a>
											<a href="{{route('admin.delete.bank',$bank->id)}}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
					
				</section>
				<!--/ page content -->
				<!-- start code here... -->
				</div><!--/middle content wrapper-->
				</div><!--/ content wrapper -->
				@endsection