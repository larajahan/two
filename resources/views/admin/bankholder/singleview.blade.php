@extends('admin.admin_layouts')
@section('admin_content')

<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->


      <div class="row"> 
        <div class="col-md-8 offset-1"> 
      <div class="panel mb-0">
        <div class="panel_header ">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Bank  Details</span>
          </div>
        </div>
        <div class="panel_body  ">
          
          
          <div class="row">
            
            <div class="col-md-8 offset-2">
              <div class="user_info">
                <div class="table-responsive">
                  <table class="table table-hover mt-2">                  
                    <tbody>
                      
                      <tr>
                        <td class="font-weight-bold">Bank Name</td>
                         <td>{{$single_view->bank_id}}</td>
                      </tr>  
                                     
                       <tr>
                         <td class="font-weight-bold">Bank Holder</td>
                         <td>{{$single_view->bank_holder_name}}</td>
                      </tr>
                      <tr>
                         <td class="font-weight-bold">Account Number</td>
                         <td>{{$single_view->account_no}}</td>
                      </tr> 
                      <tr>
                         <td class="font-weight-bold">Address</td>
                         <td>{{$single_view->address}}</td>
                      </tr>
                       <tr>
                         <td class="font-weight-bold">Known Person</td>
                         <td>{{$single_view->know_person}}</td>
                      </tr> 
                      <tr>
                         <td class="font-weight-bold">Designation</td>
                         <td>{{$single_view->designation}}</td>
                      </tr>  
                      <tr>
                         <td class="font-weight-bold">Mobile</td>
                         <td>{{$single_view->mobile}}</td>
                      </tr> 
                      <tr>
                         <td class="font-weight-bold">Opening Balance</td>
                         <td>{{$single_view->opening_blance}}</td>
                      </tr>
                      <tr>
                         <td class="font-weight-bold">Balance</td>
                         <td>{{$single_view->balance}}</td>
                      </tr>  
                        <tr>                        
                         <td><a class="btn btn-info btn-sm" href="{{url('admin/all/bankholder')}}">Back</a></td> 
                          </tr>
                    </tbody>                  
                  </table>               
            </div>         
          </div>
          
          
          
         
          
          </div> <!--/ panel body -->
          </div><!--/ panel -->
          </div>
           </div>
        </section>
        <!--/ page content -->
        <!-- start code here... -->
        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->
        @endsection