@extends('admin.admin_layouts')
@section('admin_content')
<div class="content_wrapper">
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
              
       <div class="panel_body">
       <a href="#" onclick="printDiv()" class="btn btn-info btn-sm" style="float: right;"><i class="fa fa-print"></i></a> 
          <div class="row" id="divprint">
               @for ($i = 0; $i < $howmany; $i++)
                <div class="col-lg-3">
                  <div style="padding: 5px;"><br>
                   {!! DNS1D::getBarcodeSVG($product->product_code, "C39", 1,33) !!} <br>
                   <small >{{$product->product_name}} | {{$product->sell_price}} Tk</small>
                   
                  </div>
                </div>                
                <br><hr> 
                 @endfor
            </div>
          </div> 

          
      </div>
   </section>  
  </div>
</div>
<script type="text/javascript">
  function printDiv() 
 {

  var divToPrint=document.getElementById('divprint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

 }
</script>
@endsection