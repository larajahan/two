@extends('admin.admin_layouts')
@section('admin_content')
<div class="content_wrapper">
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon">All Product</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="table-responsive">
            <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
              <thead>
                <tr>
                  <th>Product Id</th>
                  <th>Product name</th>
                  <th>Sell Price</th>
                   <th>Barcode</th>
                  <th>Qrcode</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $product)
                <tr>
            
                  <td >{{$product->product_code}}</td>               
                  <td >{{$product->product_name}}</td>                            
                  <td >{{$product->sell_price}}</td>  
                  <td>
                   sdf
                  </td>

                  <td>
                    dfds
                  </td>                            
                  <td>

                 <div class="dropdown">
                  <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Action
                  </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="{{ route('admin.barcode.set', $product->id) }}">Make Barcode</a>
                      <a class="dropdown-item" href="{{ URL::to('admin/barcode/set/'.$product->id) }}">Make Qrcode</a>
                       <a class="dropdown-item" href="{{ route('admin.product.delete', $product->id) }}" id="delete"">Delete</a>
      
                 </div>
                </div>   
              </div> 
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

          </div>
          </div> <!--/ panel body -->
          </div><!--/ panel -->
        </section>
        <!--/ page content -->
        <!-- start code here... -->

        </div><!--/middle content wrapper-->
        </div><!--/ content wrapper -->

        @endsection

       