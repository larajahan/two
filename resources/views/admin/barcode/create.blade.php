@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span>Product Image
            <span style="float: right;"><i class="fas fa-border-all"></i> Product Barcode</span>
          </div>
        </div>
        <div class="panel_body">
          <div class="row">
                 <div class="col-lg-6">
                    <img src="{{ URL::to('public/panel/product/'.$product->image)}}" style="height: 100px; width: 100px;"><br>
                     <strong class="pl-3">{{$product->product_name}}</strong>
                  </div>
                  
                  <div class="col-lg-6">
                    <div style="padding: 10px; margin-left: 20%;"><br>
                     {!! DNS1D::getBarcodeSVG($product->product_code, "C39") !!} <br>
                     <span style="margin-left: 24%;">{{$product->product_name}} | {{$product->sell_price}} Tk</span>
                    </div>
                  </div>
                  <br><hr>
                  
            </div>

          </div>  
        </div>
      </div> <!--/ panel body -->
            <div class="jumbotron" >
                <h5 align="center">Print Barcode</h5>
                <hr>
                <div class="col-lg-6 offset-4">
                    <form class="form-inline" action="{{route('admin.barcode.view')}}"  method="post">
                    @csrf
                      <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">Password</label>
                        <input type="number" class="form-control" id="howmany" placeholder="how Many Barcode" required="" name="howmany">
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                      </div>
                      <button type="submit" class="btn btn-primary mb-2">Barcode</button>
                    </form>
                </div>
              </div>
    </div><!--/ panel -->
   </section>  

                 

  </div><!--/middle content wrapper-->
</div><!--/ content wrapper -->
 .
@endsection