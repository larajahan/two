<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function employee(){
    return $this->belongsTo('App\Employee','employee_id');
}
    public function branchas(){
        return $this->belongsTo('App\User','branch');
    }
    protected $guarded = [];

}
