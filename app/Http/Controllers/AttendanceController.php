<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function searchemployee(){
        $branch = Auth::id();
        $employees =Employee::where('user_id',$branch)->get();
        return view('attendance.employee.search',compact('employees'));
    }
    public function searcempresult(Request $request){

        $month = $request->month;
        $branch = Auth::id();
        $employee = $request->employee;

        $att_month=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->orderBy('date','DESC')->get();

        $att_month_present=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','present')->orderBy('date','DESC')->get();
        $att_month_absent=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','absent')->orderBy('date','DESC')->get();
        $att_month_vacation=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','vacation')->orderBy('date','DESC')->get();

        return view('attendance.employee.month',compact('att_month','att_month_present','att_month_absent','att_month_vacation'));

    }


    //yearly search
    public function yearlySearch(){
        $branch = Auth::id();
        $employees =Employee::where('user_id',$branch)->get();
        return view('attendance.employee.yearlysearch',compact('employees'));
    }



    public function yearlyresult(Request $request){

        $year = $request->year;
        $branch = Auth::id();
        $employee = $request->employee;

        $att_month=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->orderBy('date','DESC')->get();

        $att_month_present=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','present')->orderBy('date','DESC')->get();
        $att_month_absent=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','absent')->orderBy('date','DESC')->get();
        $att_month_vacation=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','vacation')->orderBy('date','DESC')->get();

        return view('attendance.employee.month',compact('att_month','att_month_present','att_month_absent','att_month_vacation'));

    }




    public function attshow(){
        $branch = Auth::id();
        $att_today=Attendance::where('branch',$branch)->where('date',date('Y-m-d'))->get();
        return view('attendance.att_today',compact('att_today'));
    }


    public function dateedit(){

        return view('attendance.dateedit');

    }

    public function editdatesave(Request $request){
        $branch = Auth::id();
        $date = $request->date;
        $att_today=Attendance::where('branch',$branch)->where('date',$date)->get();
        return view('attendance.dateupdate',compact('att_today'));
    }

    public function datewiseupdate(Request $request){
        // return $request->all();

        foreach ($request->id as $id) {
            $data=[
                "employee_id" => $request->emp_id[$id],
                "attendance" => $request->attendence[$id],
                "date" => $request->date,
                "attendance_year" => $request->attendance_year,
                "attendance_month" => $request->attendance_month,
                "attendance_day" => $request->attendance_day,
                "attendance_day" => $request->attendance_day,
                "branch" => $request->branch[$id],
            ];
            $attendence= Attendance::where(['date' =>$request->date, 'id'=>$id])->first();
            $attendence->update($data);


        }


        if ($attendence) {
            $notification=array(
                'messege'=>'Successfully Attendence Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'messege'=>'error ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }

    }

    public function editattendace(){
        $branch = Auth::id();
        $att_today=Attendance::where('branch',$branch)->where('date',date('Y-m-d'))->get();
        return view('attendance.editattendance',compact('att_today'));
    }
    public function updateattendacesave(Request $request){
        foreach ($request->id as $id) {
            $data=[
                "employee_id" => $request->emp_id[$id],
                "attendance" => $request->attendence[$id],
                "date" => $request->date,
                "attendance_year" => $request->attendance_year,
                "attendance_month" => $request->attendance_month,
                "attendance_day" => $request->attendance_day,
                "attendance_day" => $request->attendance_day,
                "branch" => $request->branch[$id],
            ];
            $attendence= Attendance::where(['date' =>$request->date, 'id'=>$id])->first();
            $attendence->update($data);

        }


        if ($attendence) {
            $notification=array(
                'messege'=>'Successfully Attendence Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->route('maneger.att.show')->with($notification);
        }else{
            $notification=array(
                'messege'=>'error ',
                'alert-type'=>'success'
            );
            return Redirect()->route('maneger.att.show')->with($notification);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = Auth::id();
        $employees = Employee::where('user_id',$branch)->get();

        return view('attendance.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $date = $request->date;
        $branch = $request->branch;
//        $att_date=DB::table('attendances')->where('date',$date)->first();
        $att_branch=DB::table('attendances')->where('date',$date)->where('branch',$branch)->first();
        if ($att_branch) {
            $notification=array(
                'messege'=>'Today Attendence Already Taken ',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }else {

            foreach ($request->emp_id as $id) {
                $data[] = [
                    "employee_id" => $id,
                    "attendance" => $request->attendence[$id],
                    "date" => $request->date,
                    "attendance_year" => $request->attendance_year,
                    "attendance_month" => $request->attendance_month,
                    "attendance_day" => $request->attendance_day,
                    "attendance_day" => $request->attendance_day,
                    "branch" => $request->branch,



                ];
            }



            $att = DB::table('attendances')->insert($data);
            if ($att) {
                $notification = array(
                    'messege' => 'Successfully Attendence Taken ',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            } else {
                $notification = array(
                    'messege' => 'error ',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function search(){

        return view('attendance.search');

    }
    public function searchresult(Request $request){
        $branch = Auth::id();
        $date = $request->searchdate;
        $att_today=Attendance::where('branch',$branch)->where('date',$date)->get();

        return view('attendance.att_today',compact('att_today'));
    }
}
