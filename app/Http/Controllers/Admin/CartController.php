<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 use Gloudemans\Shoppingcart\Facades\Cart;
use App\Buyer;
use App\Supplier;
use App\Product;
use DB;
// use Cart;
class CartController extends Controller
{
    public function __construct()
  {
    $this->middleware('auth:admin');
  }
   public function index()
    {
      // $buyers = DB::table('buyers')
      //       ->join('products', 'buyers.product_name', '=', 'products.id')
      //       ->join('units', 'buyers.unit_id', '=', 'units.id')
      //       ->join('suppliers', 'buyers.supplier_name', '=', 'suppliers.id')
      //       ->select('buyers.*', 'products.product_name','units.name','suppliers.name')
      //       ->get();
            //return response()->json($buyers);
     return view('admin.cart.index');
    	

    }

  public function SupplierGet($supplierid)
      {
      $supp=DB::table('suppliers')->where('id',$supplierid)->select('id','supplier_id')->first();
    	return json_encode($supp);
      }

      public function productGet($productid)
      {
      $pro=DB::table('products')->where('id',$productid)->select('id','product_code','buy_price')->first();
      return json_encode($pro);
      }

       public function store(Request $request)
      {
      	  $supplier_id=$request->supplier_id;
      	  $category_id=$request->category_id;
          $item_code=$request->item_code;
          $product_name=$request->product_name;
          $qty=$request->qty;
          $mrp=$request->mrp;
          $unit_id=$request->unit_id;
          $amount=$request->amount;

           Cart::add(['id' => $item_code, 'name' => $product_name, 'price' => $mrp, 'qty' => $qty,  'weight' => $unit_id, 'options' => ['category_id' => $category_id , 'supplier_id' => $supplier_id]]);

           return Redirect()->back();

      }
      public function destroy()
      {
      	Cart::destroy();
      	return redirect()->back();
      }

     public function delete($id)
      {
       Cart::remove($id);
       return redirect()->back();
      
       }

       public function update(Request $request,$id)
       {

       	   $qty=$request->qty;
       	   $rowId=$request->rowId;
       	   Cart::update($rowId,$qty);
       	  return redirect()->back();
          
      }
           
       



  

    public function GetProduct($productid)
      {
     $buyer=DB::table('products')->where('category_id',$productid)->select('id','product_name')->get();
       
      return json_encode($buyer);
      }

      public function check()
      {
      	 $content=Cart::content();

      	 // $product_id=Cart::content()->name;
      	 //$product_id=Cart::instance('name')->content();
      	
      	
      	 return response()->json($content);
      }




  public function General(Request $request)
    {
       $id=$request->supplier_id;
       $qty=$request->qty;
       $item_code=$request->item_code;
       $product_name=$request->product_name;
       $rate=$request->rate;
       $amount=$request->amount;
       $invoice=$request->invoice;
       $date=$request->date;
       $count=$request->count;
       $total_amount=$request->total_amount;
        // echo $id;
         $supplierid=DB::table('suppliers')->where('id',$id)->first();
         return view('admin.cart.invoice',compact('supplierid','qty','item_code','product_name','rate','amount','invoice','date','count','total_amount'));
         // $pdf = PDF::loadView('admin.buyer.invoice', compact('supplierid','qty','item_code','product_name','rate','amount','invoice','date','count','total_amount'));
    
         //   return $pdf->stream('invoice.pdf');
     }
}
