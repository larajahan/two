<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;

class BranchController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth:admin');
    }

     public function addbranch()
   {
     return view('admin.branch.addbranch');
   }  
    public function insertbranch(Request $request)
   {
          $request->validate([
            'name' => 'required|max:30',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5'],            
            'address' => 'required',            
            'phone' => 'required',  
            'manager' => 'required',
            'manager_phone' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
         ]); 
          $data=array();
          $data['name']=$request->name;
          $data['email']=$request->email;
          $data['address']=$request->address;
          $data['phone']=$request->phone;
          $data['password']=Hash::make($request->password);
          $data['manager']=$request->manager;
          $data['manager_phone']=$request->manager_phone;
          $data['city']=$request->city;
          $data['zipcode']=$request->zipcode;
          $branchinsert=DB::table('users')
                        ->insert($data);

                if ($branchinsert) {           
          $notification=array(
            'messege'=>'Branch Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('/admin/all/branch')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

         }

         public function viewbranch ()
         {
         	$branches=User::all();         
         	return view('admin.branch.viewbranch',compact('branches'));

         }
        

         public function deletebranch($brance_id)
         {
           
              $delete=DB::table('users')->where('id',$brance_id)->delete();
         if ($delete) {           
             $notification=array(
            'messege'=>'Branch Delete Successfully',
            'alert-type'=>'success'
             );
              return Redirect()->back()->with($notification);
            }
         else{
             $notification=array(
             'messege'=>'Failed!',
             'alert-type'=>'error'
              );
           return Redirect()->back()->with($notification);
     }
         }

         public function editbranch ($brance_id)
         {
         	  $branchedit= DB::table('users')->where('id',$brance_id)->first();
               return view('admin.branch.edit',compact('branchedit'));
         }

         public function updatebranch(Request $request)
         {
           
            $id=$request->id;

         	$data=array();
          $data['name']=$request->name;
          $data['email']=$request->email;
          $data['address']=$request->address;
          $data['phone']=$request->phone;
          $data['password']=Hash::make($request->password);
          $data['manager']=$request->manager;
          $data['manager_phone']=$request->manager_phone;
          $data['city']=$request->city;
          $data['zipcode']=$request->zipcode;
          $branchupdate=DB::table('users')->where('id',$id)
                        ->update($data);

            if ($branchupdate) {           
          $notification=array(
            'messege'=>'Branch Updated Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('/admin/all/branch')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
           

         }
}
