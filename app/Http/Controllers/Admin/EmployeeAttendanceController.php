<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeAttendanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
   public function search(){
       $branchs = User::all();
       return view('admin.attendance.employee.search',compact('branchs'));
   }

   public function searchresult(Request $request){
       $month = $request->month;
       $branch = $request->branch;
       $employee = $request->employee;

       $att_month=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->orderBy('date','DESC')->get();
       $att_month_present=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','present')->orderBy('date','DESC')->get();
       $att_month_absent=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','absent')->orderBy('date','DESC')->get();
       $att_month_vacation=Attendance::where('attendance_month',$month)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','vacation')->orderBy('date','DESC')->get();

       return view('admin.attendance.employee.month',compact('att_month','att_month_present','att_month_absent','att_month_vacation'));
   }

    public function searchemp($branch_id){
        $employee = DB::table("employees")->where("user_id",$branch_id)->select("name","id")->get();
        return json_encode($employee);
    }

    public function yearlySearch(){
        $branchs = User::all();
        return view('admin.attendance.employee.yearly',compact('branchs'));

    }

    public function yearlyresult(Request $request){
        $year = $request->year;
        $branch = $request->branch;
        $employee = $request->employee;

        $att_month=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->orderBy('date','DESC')->get();
        $att_month_present=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','present')->orderBy('date','DESC')->get();
        $att_month_absent=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','absent')->orderBy('date','DESC')->get();
        $att_month_vacation=Attendance::where('attendance_year',$year)->where('branch',$branch)->where('employee_id',$employee)->where('attendance','vacation')->orderBy('date','DESC')->get();

       return view('admin.attendance.employee.month',compact('att_month','att_month_present','att_month_absent','att_month_vacation'));

    }
}
