<?php

namespace App\Http\Controllers\Admin;
use App\Bankholder;
use App\Bank;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BankholderController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function addbankholder()
    {   
        $bankholders=Bank::all(); 
        return view('admin.bankholder.addbankhold',compact('bankholders'));
    }

    public function insertbank(Request $request)
    {
         $request->validate([
            'bank_holder_name' => 'required|max:30',  
            'account_no' => 'required',            
            'address' => 'required',  
            'know_person' => 'required',
            'designation' => 'required',
            'mobile' => 'required|numeric|unique:bankholders',
            'opening_blance' => 'required|numeric',
            'balance' => 'required|numeric',
         ]); 
          $data=array();
          $data['bank_id']=$request->bank_id;         
          $data['bank_holder_name']=$request->bank_holder_name;         
          $data['account_no']=$request->account_no;
          $data['address']=$request->address;
          $data['know_person']=$request->know_person;
          $data['designation']=$request->designation;
          $data['mobile']=$request->mobile;   
          $data['opening_blance']=$request->opening_blance;
          $data['balance']=$request->balance;
          $branchinsert=DB::table('bankholders')
                        ->insert($data);

            if ($branchinsert) {           
	          $notification=array(
	            'messege'=>'Bank Holder Added Successfully',
	            'alert-type'=>'success'
	             );
	           return Redirect()->to('admin/all/bankholder')->with($notification);
           }
     else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

    }
    public function viewbank()
    {
        // $bankholders=Bankholder::all(); 
        $bankholders = DB::table('bankholders')
            ->join('banks', 'bankholders.bank_id', '=', 'banks.id')            
            ->select('bankholders.*', 'banks.bank_name')
            ->get();            
         	return view('admin.bankholder.viewbank',compact('bankholders'));
    }
    public function deletebankholder($bankholder_id)
    {
        $delete=DB::table('bankholders')->where('id',$bankholder_id)->delete();
         if ($delete) {           
             $notification=array(
            'messege'=>'Bank Holder Delete Successfully',
            'alert-type'=>'success'
             );
              return Redirect()->back()->with($notification);
            }
         else{
             $notification=array(
             'messege'=>'Failed!',
             'alert-type'=>'error'
              );
           return Redirect()->back()->with($notification);
     }
    }
    public function editbank($bankholder_id)
    {    
        
              
         $bankholderedit= DB::table('bankholders')->where('id',$bankholder_id)->first();
               return view('admin.bankholder.edit',compact('bankholderedit'));
    }
    public function updatebank(Request $request)
    {
           $id=$request->id;
          $data=array();
               
          $data['bank_holder_name']=$request->bank_holder_name;         
          $data['account_no']=$request->account_no;
          $data['address']=$request->address;
          $data['know_person']=$request->know_person;
          $data['designation']=$request->designation;
          $data['mobile']=$request->mobile;   
          $data['opening_blance']=$request->opening_blance;
          $data['balance']=$request->balance;
          $updatebankholder=DB::table('bankholders')->where('id',$id)
                        ->update($data);

            if ($updatebankholder) {           
          $notification=array(
            'messege'=>'Bank Holder Updated Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('admin/all/bankholder')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
    }

     public function bankholdersingleview($bankholder_id)
             {
              $single_view=Bankholder::where('id',$bankholder_id)->first();
               return view('admin.bankholder.singleview',compact('single_view'));

             }

}
