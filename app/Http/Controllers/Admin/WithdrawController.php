<?php

namespace App\Http\Controllers\Admin;
use App\Bank;
use App\Deposit;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WithdrawController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function addwithdraw ()
    {
    	$banks = Bank::all();    
                       
         return view('admin.withdraw.addwithdraw',compact('banks'));
    }

    public function insertwithdraw (Request $request)
    {      

               $request->validate([
            'bank' => 'required',  
            'account_holder_name' => 'required',            
            'pay_mode' => 'required',            
            'payment_amount' => 'required|numeric',
            
         ]);
    	    $data=array();               
          $data['invoice_no']=$request->invoice_no;         
          $data['date']=$request->date;
          $data['bank']=$request->bank;
          $data['account_no']=$request->account_no;
          $data['account_holder_name']=$request->account_holder_name;
          $data['pay_mode']=$request->pay_mode;   
          $data['recive_bank']=$request->recive_bank;   
          $data['recive_holder_name']=$request->recive_holder_name;   
          $data['check_no']=$request->check_no;   
          $data['payment_amount']=$request->payment_amount;
          $data['remarks']=$request->remarks;

          $withdraw=DB::table('withdraws')
                        ->insert($data);

                         if ($withdraw) {           
            $notification=array(
              'messege'=>'Withdraw Added Successfully',
              'alert-type'=>'success'
               );
             return Redirect()->to('admin/all/withdraw')->with($notification);
           }
     else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
    }

    public function viewwithdraw ()
    { 
    	

    	 $withdraws = DB::table('withdraws')
            ->leftJoin('banks', 'withdraws.bank','banks.id')            
            ->leftJoin('banks as otherbank', 'withdraws.recive_bank','otherbank.id')            
            ->leftJoin('bankholders', 'withdraws.account_holder_name','bankholders.id')            
            ->leftJoin('bankholders as reciveholders', 'withdraws.recive_holder_name','reciveholders.id')            
            ->select('withdraws.*', 'banks.bank_name','otherbank.bank_name as recieve_bank','bankholders.bank_holder_name as sender_holder_name','reciveholders.bank_holder_name as whorecieve')
            ->get(); 
    	return view('admin.withdraw.view',compact('withdraws'));
    }

      public function deletewithdraw ($withdraw_id)
       {
        $delete=DB::table('withdraws')->where('id',$withdraw_id)->delete();
       
         if ($delete) {           
             $notification=array(
            'messege'=>'Withdraw Delete Successfully',
            'alert-type'=>'success'
             );
              return Redirect()->back()->with($notification);
            }
         else{
             $notification=array(
             'messege'=>'Failed!',
             'alert-type'=>'error'
              );
           return Redirect()->back()->with($notification);
        }
    }
       public function editwithdraw ($withdraw_id)
    {    
        
              
         $withdraws=DB::table('withdraws')->where('id',$withdraw_id)->first();
           // return response()->json($withdraws);
               return view('admin.withdraw.edit',compact('withdraws'));
    }


    public function updatewithdraw (Request $request)
    {
    	  $id=$request->id;
    	  $data=array();               
          $data['invoice_no']=$request->invoice_no;         
          $data['date']=$request->date;
          $data['bank']=$request->bank;
          $data['account_no']=$request->account_no;
          $data['account_holder_name']=$request->account_holder_name;
          $data['pay_mode']=$request->pay_mode;   
          $data['recive_bank']=$request->recive_bank;   
          $data['recive_holder_name']=$request->recive_holder_name;   
          $data['check_no']=$request->check_no;   
          $data['payment_amount']=$request->payment_amount;
          $data['remarks']=$request->remarks;
           $updatewithdraw=DB::table('withdraws')->where('id',$id)
                        ->update($data);

           if ($updatewithdraw) {           
          $notification=array(
            'messege'=>'Deposits Updated Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->to('admin/all/withdraw')->with($notification);
            }
          else{
            $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
            return Redirect()->back()->with($notification);
           }

    }


   public function withdrawsingleview ($withdraw_id)
      {

      	 $single_view = DB::table('withdraws')
            ->leftJoin('banks', 'withdraws.bank','banks.id')            
            ->leftJoin('banks as otherbank', 'withdraws.recive_bank','otherbank.id')            
            ->leftJoin('bankholders', 'withdraws.account_holder_name','bankholders.id')            
            ->leftJoin('bankholders as reciveholders', 'withdraws.recive_holder_name','reciveholders.id')            
            ->select('withdraws.*', 'banks.bank_name','otherbank.bank_name as recieve_bank','bankholders.bank_holder_name as sender_holder_name','reciveholders.bank_holder_name as whorecieve')
             ->where('withdraws.id',$withdraw_id)
            ->first(); 
            // print_r($single_view);
        // $single_view=DB::table('withdraws')->where('id',$withdraw_id)->first();
        return view('admin.withdraw.singleview',compact('single_view'));            
      }


}
