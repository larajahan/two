<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $employees = Employee::all();
        return view('admin.employee.index', compact('employees'));
    }


    public function create()
    {
        $branchs = User::all();
        return view('admin.employee.create', compact('branchs'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:30',
            'email' => 'required|email',
            'address' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,bmp',
            'mobile' => 'required|numeric',
            'fax' => 'required',
            'contact_person' => 'required',
            'position' => 'required',
            'company_person' => 'required',
            'bank_name' => 'required',
            'account_number' => 'required',
            'account_name' => 'required',
            'branch' => 'required',
            'salary' => 'required',
            'perday' => 'required',
        ]);

        $data = array();
        $data['user_id'] = $request->branch;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['address'] = $request->address;
        $data['mobile'] = $request->mobile;
        $data['fax'] = $request->fax;
        $data['contact_person'] = $request->contact_person;
        $data['position'] = $request->position;
        $data['company_person'] = $request->company_person;
        $data['bank_name'] = $request->bank_name;
        $data['account_number'] = $request->account_number;
        $data['account_name'] = $request->account_name;
        $data['branch'] = $request->bank_branch;
        $data['salary'] = $request->salary;
        $data['perday'] = $request->perday;
        $image = $request->file('image');
        if ($image) {
            $image_name = hexdec(uniqid());

            $filename = $image_name . "." . $image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save('public/panel/employee/' . $filename);
            $data['image'] = $filename;

            $empyinsert = DB::table('employees')
                ->insertGetId($data);
            $day = date('d');
            $month = date('Y');
            $employee_id = 'S-' . $empyinsert . $day . $month;
            $employee = DB::table('employees')->where('id', $empyinsert)->update(['employee_id' => $employee_id]);
            $notification = array(
                'messege' => 'Successfully Employee Inserted ',
                'alert-type' => 'success'
            );
            return Redirect()->back()->with($notification);
        } else {

            $empyinsert = DB::table('employees')
                ->insertGetId($data);
            $day = date('d');
            $month = date('Y');
            $employee_id = 'C-' . $empyinsert . $day . $month;
            $employee = DB::table('employees')->where('id', $empyinsert)->update(['employee_id' => $employee_id]);
            $notification = array(
                'messege' => 'Successfully Customer Inserted ',
                'alert-type' => 'success'
            );
            return Redirect()->back()->with($notification);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee=DB::table('employees')->where('id',$id)->first();
        return view('admin.employee.view',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchs = User::all();
        $employee = DB::table('employees')->where('id', $id)->first();
        return view('admin.employee.edit', compact('employee', 'branchs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old = $request->oldpic;

        if ($request->hasFile('image')) {
            unlink('public/panel/employee/' . $old);
            $photo = $request->image;
            $filename = hexdec(uniqid()) . "." . $photo->getClientOriginalExtension();
            Image::make($photo)->resize(200, 200)->save('public/panel/employee/' . $filename);

            Employee::where('id', $id)->update([
                'image' => $filename
            ]);

            $notification = array(
                'messege' => 'Employee Updated Successfully',
                'alert-type' => 'success'
            );
            return Redirect()->back()->with($notification);
        }
        $employee = Employee::where('id', $id)->update([
            'user_id' => $request->branch,
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'mobile' => $request->mobile,
            'fax' => $request->fax,
            'contact_person' => $request->contact_person,
            'position' => $request->position,
            'bank_name' => $request->bank_name,
            'account_number' => $request->account_number,
            'account_name' => $request->account_name,
            'branch' => $request->bank_branch,
            'salary' => $request->salary,
            'perday' => $request->perday,
        ]);
        $notification = array(
            'messege' => 'Employee Updated Successfully',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee=DB::table('employees')->where('id',$id)->first();
        $image=$employee->image;
        $delete=DB::table('employees')->where('id',$id)->delete();

        if ($delete) {
            unlink('public/panel/employee/'.$image);
            $notification=array(
                'messege'=>'Employee Delete Successfully',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'messege'=>'Failed!',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
    }
    }
}