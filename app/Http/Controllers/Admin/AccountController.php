<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function employeeDetails(){
        $branchs = User::all();
        return view('admin.account.salary',compact('branchs'));
    }

    public function empInfo($employee_id){


        $emp = Employee::where('id',$employee_id)->with('attendances')->first();
       // $emp_info = Employee::where('id',$employee_id)->first();

        return json_encode($emp);

    }
}
