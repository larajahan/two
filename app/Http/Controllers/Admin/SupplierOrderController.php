<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class SupplierOrderController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth:admin');
    }
     public function store(Request $request )
    {
    	// $request->validate([
     //    'item_code' => 'required|max:30',
     //    'item_name' => 'required',
     //    'unit' => 'required',
     //    'qty' => 'required,
     //    'rate' => 'required',
     //    'subtotal' => 'required',    
     //     ]); 

       $data=array();
       $data['item_code']=$request->item_code;
       $data['item_name']=$request->item_name;
       $data['unit']=$request->unit;
       $data['qty']=$request->qty;
       $data['rate']=$request->rate;
       $data['subtotal']=$request->subtotal;

       $order=DB::table('supplier_orders')
                                    ->insertGetId($data);
            $day=date('d');
            $month=date('m');
            $month=date('Y');
            $order_id='S-'.$order.$day.$month.$year; 
            $order=DB::table('supplier_orders')->where('id',$order)->update(['order_id' => $order_id]);
             $notification=array(
                 'messege'=>'Successfully supplier_orders Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->back()->with($notification);                  
    
    }
    
}
