<?php

namespace App\Http\Controllers\Admin;
use App\Bank;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BankController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function addbank()
    {
        $banks=Bank::all(); 
        return view('admin.bank.addbank',compact('banks'));
    }

    public function insertbank(Request $request)
    {
         $request->validate([
            'bank_name' => 'required|unique:banks|max:25', 
         ]); 
          $data=array();
          $data['bank_name']=$request->bank_name;
          $branchinsert=DB::table('banks')
                        ->insert($data);

            if ($branchinsert) {           
	          $notification=array(
	            'messege'=>'Bank Added Successfully',
	            'alert-type'=>'success'
	             );
	           return Redirect()->back()->with($notification);
           }
     else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }

    }

    public function deletebank($bank_id)
    {
        $delete=DB::table('banks')->where('id',$bank_id)->delete();
         if ($delete) {           
             $notification=array(
            'messege'=>'Bank Delete Successfully',
            'alert-type'=>'success'
             );
              return Redirect()->back()->with($notification);
            }
         else{
             $notification=array(
             'messege'=>'Failed!',
             'alert-type'=>'error'
              );
           return Redirect()->back()->with($notification);
     }
    }
    public function editbank($bank_id)
    {
         $bankedit= DB::table('banks')->where('id',$bank_id)->first();
               return view('admin.bank.edit',compact('bankedit'));
    }
    public function updatebank(Request $request)
    {
           $id=$request->id;
          $data=array();
          $data['bank_name']=$request->bank; 
          $bankupdate=DB::table('banks')->where('id',$id)
                        ->update($data);
            if ($bankupdate) {           
          $notification=array(
            'messege'=>'Bank Updated Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.add.bank')->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
    }

    
}
