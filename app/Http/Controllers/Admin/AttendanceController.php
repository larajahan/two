<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function attendace(Request $request){


        $date = $request->date;
        $branch = $request->branch;
       // $att_date=DB::table('attendances')->where('date',$date)->first();

        $att_branch=DB::table('attendances')->where('branch',$branch)->where('date',$date)->first();
        //dd($att_branch);


        //return $att_branch;
        if ($att_branch) {
            $notification=array(
                'messege'=>'Today Attendence Already Taken ',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }else {

            foreach ($request->emp_id as $id) {
                $data[] = [
                    "employee_id" => $id,
                    "attendance" => $request->attendence[$id],
                    "date" => $request->date,
                    "attendance_year" => $request->attendance_year,
                    "attendance_month" => $request->attendance_month,
                    "attendance_day" => $request->attendance_day,
                    "attendance_day" => $request->attendance_day,
                    "branch" => $request->branch,



                ];
            }



            $att = DB::table('attendances')->insert($data);
            if ($att) {
                $notification = array(
                    'messege' => 'Successfully Attendence Taken ',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            } else {
                $notification = array(
                    'messege' => 'error ',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            }
        }
    }


    public function attendaceshow(){

        $att_today=Attendance::where('date',date('Y-m-d'))->get();
        return view('admin.attendance.att_today',compact('att_today'));

    }

    public function editattendace(){
        $att_today=Attendance::where('date',date('Y-m-d'))->get();
        return view('admin.attendance.editattendance',compact('att_today'));

    }


    public function updateattendace(Request $request){
      // return $request->all();

        foreach ($request->id as $id) {
            $data=[
                "employee_id" => $request->emp_id[$id],
                "attendance" => $request->attendence[$id],
                "date" => $request->date,
                "attendance_year" => $request->attendance_year,
                "attendance_month" => $request->attendance_month,
                "attendance_day" => $request->attendance_day,
                "attendance_day" => $request->attendance_day,
                "branch" => $request->branch[$id],
            ];
            $attendence= Attendance::where(['date' =>$request->date, 'id'=>$id])->first();
            $attendence->update($data);

        }


        if ($attendence) {
            $notification=array(
                'messege'=>'Successfully Attendence Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'messege'=>'error ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }

    }

    public function branchupdateattendace(Request $request){
        // return $request->all();

        foreach ($request->id as $id) {
            $data=[
                "employee_id" => $request->emp_id[$id],
                "attendance" => $request->attendence[$id],
                "date" => $request->date,
                "attendance_year" => $request->attendance_year,
                "attendance_month" => $request->attendance_month,
                "attendance_day" => $request->attendance_day,
                "attendance_day" => $request->attendance_day,
                "branch" => $request->branch[$id],
            ];
            $attendence= Attendance::where(['date' =>$request->date, 'id'=>$id])->first();
            $attendence->update($data);

        }


        if ($attendence) {
            $notification=array(
                'messege'=>'Successfully Attendence Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'messege'=>'error ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }

    }






    public function attendacebranch(){
        $branchs = User::all();
        return view('admin.attendance.branch',compact('branchs'));
    }

//datewise attendance update

    public function editdateattendace(){
        $branchs = User::all();
        return view('admin.attendance.dateedit',compact('branchs'));
    }
    public  function editdateattendacesave(Request $request){
        $branch = $request->branch;
        $date = $request->date;
        $att_today=Attendance::where('branch',$branch)->where('date',$date)->get();
       return view('admin.attendance.datewiseupdate',compact('att_today'));
    }


    public function datewiseupdate(Request $request){
        // return $request->all();

        foreach ($request->id as $id) {
            $data=[
                "employee_id" => $request->emp_id[$id],
                "attendance" => $request->attendence[$id],
                "date" => $request->date,
                "attendance_year" => $request->attendance_year,
                "attendance_month" => $request->attendance_month,
                "attendance_day" => $request->attendance_day,
                "attendance_day" => $request->attendance_day,
                "branch" => $request->branch[$id],
            ];
            $attendence= Attendance::where(['date' =>$request->date, 'id'=>$id])->first();
            $attendence->update($data);


        }


        if ($attendence) {
            $notification=array(
                'messege'=>'Successfully Attendence Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'messege'=>'error ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }

    }



    public function editattendacebranch (){
        $branchs = User::all();
        return view('admin.attendance.editbranch',compact('branchs'));

    }

    public  function editbranchattendacesave(Request $request){
        $branch = $request->branch;
        $att_today=Attendance::where('branch',$branch)->where('date',date('Y-m-d'))->get();
        return view('admin.attendance.editattendancebranch',compact('att_today'));
    }

    public function branchattendacesave(Request $request){
        $branch = $request->branch;
        $att_today=Attendance::where('branch',$branch)->where('date',date('Y-m-d'))->get();
        return view('admin.attendance.att_today',compact('att_today'));

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branchs = User::all();
        return view('admin.attendance.index',compact('branchs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barnch = $request->branch;
        $employees = Employee::where('user_id',$barnch)->get();
        return view('admin.attendance.view',compact('employees'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(){

        $branchs = User::all();

        return view('admin.attendance.search',compact('branchs'));

    }
    public function searchresult(Request $request){
        $date = $request->searchdate;
        $att_today=Attendance::where('date',$date)->get();
        return view('admin.attendance.att_today',compact('att_today'));

    }
    public  function searchbranchdate(Request $request){
        $date = $request->searchdate;
        $branch = $request->branch;
        $att_today=Attendance::where('date',$date)->where('branch',$branch)->get();
        return view('admin.attendance.att_today',compact('att_today'));
    }
}
