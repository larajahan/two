<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
use App\Buyer;
use App\Supplier;
use DB;
class BuyerController extends Controller
{
    
 public function __construct()
  {
    $this->middleware('auth:admin');
  }
   

 public function index()
    {
      $buyers = DB::table('buyers')
            ->join('products', 'buyers.product_name', '=', 'products.id')
            ->join('units', 'buyers.unit_id', '=', 'units.id')
            ->join('suppliers', 'buyers.supplier_name', '=', 'suppliers.id')
            ->select('buyers.*', 'products.product_name','units.name','suppliers.name')
            ->get();
     return view('admin.buyer.index',compact('buyers'));
    }

     public function view()
    {
      $supplierid = DB::table('buyers')
            ->join('products', 'buyers.product_name', '=', 'products.id')
            ->join('units', 'buyers.unit_id', '=', 'units.id')
            ->select('buyers.*', 'products.product_name','units.name')
            ->get();
     return view('admin.buyer.invoice',compact('supplierid'));
     }


      public function SupplierGet($supplierid)
      {
      $supp=DB::table('suppliers')->where('id',$supplierid)->select('id','supplier_id','mobile','address')->first();
    	return json_encode($supp);
      }

      public function productGet($productid)
      {
      $pro=DB::table('products')->where('id',$productid)->select('id','product_code','buy_price')->first();
      return json_encode($pro);
      }


      public function store(Request $request)
      {

         $request->validate([
            'invoice' => 'required|max:30',
            'date' => 'required',
            'supplier_name' => 'required',
            'supplierid' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'category_id' => 'required',
            'item_code' => 'required',
            'product_name' => 'required',
            'rate' => 'required',
            'mrp' => 'required',
            'unit_id' => 'required',
            'qty' => 'required',
            'amount' => 'required',
            'disc' => 'nullable',
            'tax' => 'nullable',
            'company' => 'required',
            
         ]); 
          $data=array();
          $data['invoice']=$request->invoice;
          $data['date']=$request->date;
          $data['supplier_name']=$request->supplier_name;
          $data['supplierid']=$request->supplierid;
          $data['address']=$request->address;
          $data['mobile']=$request->mobile;
          $data['category_id']=$request->category_id;
          $data['company']=$request->company;
          $data['item_code']=$request->item_code;
          $data['product_name']=$request->product_name;
          $data['rate']=$request->rate;
          $data['mrp']=$request->mrp;
          $data['unit_id']=$request->unit_id;
          $data['qty']=$request->qty;
          $data['amount']=$request->amount;
          $data['disc']=$request->disc;
          $data['tax']=$request->tax;
          $buyer=DB::table('buyers')
                        ->insert($data);
                if ($buyer) {           
          $notification=array(
            'messege'=>'buyer Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
        }
      }


    public function General(Request $request)
    {
       $id=$request->supplier_id;
       $qty=$request->qty;
       $item_code=$request->item_code;
       $product_name=$request->product_name;
       $rate=$request->rate;
       $amount=$request->amount;
       $invoice=$request->invoice;
       $date=$request->date;
       $count=$request->count;
       $total_amount=$request->total_amount;
        // echo $id;
         $supplierid=DB::table('suppliers')->where('id',$id)->first();
         return view('admin.buyer.invoice',compact('supplierid','qty','item_code','product_name','rate','amount','invoice','date','count','total_amount'));
         // $pdf = PDF::loadView('admin.buyer.invoice', compact('supplierid','qty','item_code','product_name','rate','amount','invoice','date','count','total_amount'));
    
         //   return $pdf->stream('invoice.pdf');
     }

   
 



   public function delete($id)
    {
      $delete=DB::table('buyers')->where('id',$id)->delete();
     if ($delete) {           
          $notification=array(
            'messege'=>'buyers delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
        
    }


       
    public function GetProduct($productid)
      {
     $buyer=DB::table('products')->where('category_id',$productid)->select('id','product_name')->get();
       
      return json_encode($buyer);
      }


public function update(Request $request, $id)
  {
    
        $rate=$request->rate;
        $qty=$request->qty;

        $amount=$rate * $qty;

        
        $data=array();
        $data['qty'] =$request->qty;
        $data['amount'] =$amount;
        DB::table('buyers')->where('id',$id)->update($data);
        $notification=array(
            'messege'=>'Updated!',
            'alert-type'=>'success'
             );
           return Redirect()->back();
      
     }
    
  
}
