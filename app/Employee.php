<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
     protected $fillable=['name','email','address','image','mobile','fax','contact_person','position','company_person','bank_name','account_number','account_name','branch','opening_balance','salary','perday'];


     public function category()
  {
    return $this->belongsTo(Category::class);
  }

    public function branchss(){
        return $this->belongsTo('App\User','user_id');
    }
    public function attendances(){
         return $this->hasMany('App\Attendance','employee_id');
    }

}
