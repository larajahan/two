<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = ['date', 'sender_bank', 'sender_account_holder','sender_account_no','sender_account_address','pay_mode','recive_bank','recive_holder_name','check_no','payment_amount','remarks'];
}
