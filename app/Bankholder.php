<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bankholder extends Model
{
    protected $fillable = ['bank_name', 'bank_holder_name', 'account_no','address','know_person','designation','mobile','opening_blance','balance'];
}
